**Notebooks for linear regression on coherence data**

---

## Repository Format

This folder currently (10 Dec 2019) contains 4 notebooks, each which completes regression in different data or in different ways:

1. CV_ols_block_fitting_all_electrodes - linear regression to predict change in coherence of all connections of array

2. CV_ols_block_fitting_all_electrodes-classification - same as (1), but with thresholds defined for the predicted regression results to make the model a classification model

3. CV_ols_block_fitting_all_electrodes-granger - same as (1), but for granger causality data

4. CV_ols_block_fitting_paired_sites - linear regression to predict chance in coherence between only stimulated electrodes

---