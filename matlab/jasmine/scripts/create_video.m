%% find good electrodes
%load('MonkeyG_20150911_Session2_S1.mat');
date='20160624 Session 4 ';

% define good channels
good_chl=good_chl_0624_session34;
min_coh=0.2;

% define laser location
laser=[37 40];
laser_rec=[];

% write video and difine video properties
v = VideoWriter('20160624 Session 4 Coherence Map.avi');
v.FrameRate = 3;
open(v)

S1_good=[];
for m = 1:length(good_chl)
    ind=find(S1==good_chl(m));
    S1_good=[S1_good S1(ind)];
end

M1_good=[];
for n = 1:length(good_chl)
    ind=find(M1==good_chl(n));
    M1_good=[M1_good M1(ind)];
end

S1=S1_good;
M1=M1_good;

% define parameters
cond_length=size(CondBlock1,1);
rec_length=size(RecBlock1,1);
good_coords=coords_xy(good_chl,:);


%% evaluate coherence every 20 sec

% recording block 1
for n=1:rec_length
    adj_rec=squeeze(mean(RecBlock1(n,:,:,9:17),4));
    adj_rec=adj_rec(good_chl,good_chl);
    adj_rec=adj_rec-eye(size(adj_rec));
    thresh_adj_rec=threshold_adj(adj_rec,good_coords,0.2,5);
    %collect coherence for all 20sec blocks into 3D matrix
    % all_frames_adj_cond(:,:,n)=thresh_adj_cond;
    %plot each 20sec block
    figure;
    fig = gcf; % current figure handle
    fig.Color = [1 1 1];
    wgPlot_jz(thresh_adj_rec,good_coords,coords_xy,M1,S1,1,min_coh,laser_rec,date,'RecBlock1','edgeColorMap',map,'edgeWidth',1.6);
    frame=getframe(fig);
    writeVideo(v,frame);
end
%% conditioning block1
for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock1(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    thresh_adj_cond=threshold_adj(adj_cond,good_coords,0.2,5);
    %collect coherence for all 20sec blocks into 3D matrix
    % all_frames_adj_cond(:,:,n)=thresh_adj_cond;
    %plot each 20sec block
    figure;
    fig = gcf;
    fig.Color = [1 1 1];
    wgPlot_jz(thresh_adj_cond,good_coords,coords_xy,M1,S1,1,min_coh,laser,date,'CondBlock1','edgeColorMap',map,'edgeWidth',1.6);
    frame=getframe(fig);
    writeVideo(v,frame);
end
close all;

%% recording block 2
for n=1:rec_length
    adj_rec=squeeze(mean(RecBlock2(n,:,:,9:17),4));
    adj_rec=adj_rec(good_chl,good_chl);
    adj_rec=adj_rec-eye(size(adj_rec));
    thresh_adj_rec=threshold_adj(adj_rec,good_coords,0.2,5);
    %collect coherence for all 20sec blocks into 3D matrix
    % all_frames_adj_cond(:,:,n)=thresh_adj_cond;
    %plot each 20sec block
    figure;
    fig = gcf;
    fig.Color = [1 1 1];
    wgPlot_jz(thresh_adj_rec,good_coords,coords_xy,M1,S1,1,min_coh,laser_rec,date,'RecBlock2','edgeColorMap',map,'edgeWidth',1.6);
    frame=getframe(fig);
    writeVideo(v,frame);
end

%conditioning block2
for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock2(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    thresh_adj_cond=threshold_adj(adj_cond,good_coords,0.2,5);
    %collect coherence for all 20sec blocks into 3D matrix
    % all_frames_adj_cond(:,:,n)=thresh_adj_cond;
    %plot each 20sec block
    figure;
    fig = gcf;
    fig.Color = [1 1 1];
    wgPlot_jz(thresh_adj_cond,good_coords,coords_xy,M1,S1,1,min_coh,laser,date,'CondBlock2','edgeColorMap',map,'edgeWidth',1.6);
    frame=getframe(gcf);
    writeVideo(v,frame);
end
close all;

%recording block 3
for n=1:rec_length
    adj_rec=squeeze(mean(RecBlock3(n,:,:,9:17),4));
    adj_rec=adj_rec(good_chl,good_chl);
    adj_rec=adj_rec-eye(size(adj_rec));
    thresh_adj_rec=threshold_adj(adj_rec,good_coords,0.2,5);
    %collect coherence for all 20sec blocks into 3D matrix
    % all_frames_adj_cond(:,:,n)=thresh_adj_cond;
    %plot each 20sec block
    figure;
    fig = gcf;
    fig.Color = [1 1 1];
    wgPlot_jz(thresh_adj_rec,good_coords,coords_xy,M1,S1,1,min_coh,laser_rec,date,'RecBlock3','edgeColorMap',map,'edgeWidth',1.6);
    frame=getframe(gcf);
    writeVideo(v,frame);
end

%conditioning block 3
for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock3(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    thresh_adj_cond=threshold_adj(adj_cond,good_coords,0.2,5);
    %collect coherence for all 20sec blocks into 3D matrix
    % all_frames_adj_cond(:,:,n)=thresh_adj_cond;
    %plot each 20sec block
    figure;
    fig = gcf;
    fig.Color = [1 1 1];
    wgPlot_jz(thresh_adj_cond,good_coords,coords_xy,M1,S1,1,min_coh,laser,date,'CondBlock3','edgeColorMap',map,'edgeWidth',1.6);
    frame=getframe(gcf);
    writeVideo(v,frame);
end
close all;

%recording block 4
for n=1:rec_length
    adj_rec=squeeze(mean(RecBlock4(n,:,:,9:17),4));
    adj_rec=adj_rec(good_chl,good_chl);
    adj_rec=adj_rec-eye(size(adj_rec));
    thresh_adj_rec=threshold_adj(adj_rec,good_coords,0.2,5);
    %collect coherence for all 20sec blocks into 3D matrix
    % all_frames_adj_cond(:,:,n)=thresh_adj_cond;
    %plot each 20sec block
    figure;
    fig = gcf;
    fig.Color = [1 1 1];
    wgPlot_jz(thresh_adj_rec,good_coords,coords_xy,M1,S1,1,min_coh,laser_rec,date,'RecBlock4','edgeColorMap',map,'edgeWidth',1.6);
    frame=getframe(gcf);
    writeVideo(v,frame);
end

%conditioning block 4
for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock4(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    thresh_adj_cond=threshold_adj(adj_cond,good_coords,0.2,5);
    %collect coherence for all 20sec blocks into 3D matrix
    % all_frames_adj_cond(:,:,n)=thresh_adj_cond;
    %plot each 20sec block
    figure;
    fig = gcf;
    fig.Color = [1 1 1];
    wgPlot_jz(thresh_adj_cond,good_coords,coords_xy,M1,S1,1,min_coh,laser,date,'CondBlock4','edgeColorMap',map,'edgeWidth',1.6);
    frame=getframe(gcf);
    writeVideo(v,frame);
end
close all;

%recording block 5
for n=1:rec_length
    adj_rec=squeeze(mean(RecBlock5(n,:,:,9:17),4));
    adj_rec=adj_rec(good_chl,good_chl);
    adj_rec=adj_rec-eye(size(adj_rec));
    thresh_adj_rec=threshold_adj(adj_rec,good_coords,0.2,5);
    %collect coherence for all 20sec blocks into 3D matrix
    % all_frames_adj_cond(:,:,n)=thresh_adj_cond;
    %plot each 20sec block
    figure;
    fig = gcf;
    fig.Color = [1 1 1];
    wgPlot_jz(thresh_adj_rec,good_coords,coords_xy,M1,S1,1,min_coh,laser_rec,date,'RecBlock5','edgeColorMap',map,'edgeWidth',1.6);
    frame=getframe(gcf);
    writeVideo(v,frame);
end

%conditioning block 5
for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock5(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    thresh_adj_cond=threshold_adj(adj_cond,good_coords,0.2,5);
    %collect coherence for all 20sec blocks into 3D matrix
    % all_frames_adj_cond(:,:,n)=thresh_adj_cond;
    %plot each 20sec block
    figure;
    fig = gcf;
    fig.Color = [1 1 1];
    wgPlot_jz(thresh_adj_cond,good_coords,coords_xy,M1,S1,1,min_coh,laser,date,'CondBlock5','edgeColorMap',map,'edgeWidth',1.6);
    frame=getframe(gcf);
    writeVideo(v,frame);
end

%recording block 6
for n=1:rec_length
    adj_rec=squeeze(mean(RecBlock6(n,:,:,9:17),4));
    adj_rec=adj_rec(good_chl,good_chl);
    adj_rec=adj_rec-eye(size(adj_rec));
    thresh_adj_rec=threshold_adj(adj_rec,good_coords,0.2,5);
    %collect coherence for all 20sec blocks into 3D matrix
    % all_frames_adj_cond(:,:,n)=thresh_adj_cond;
    %plot each 20sec block
    figure;
    fig = gcf;
    fig.Color = [1 1 1];
    wgPlot_jz(thresh_adj_rec,good_coords,coords_xy,M1,S1,1,min_coh,laser_rec,date,'RecBlock6','edgeColorMap',map,'edgeWidth',1.6);
    frame=getframe(gcf);
    writeVideo(v,frame);
end
%}
 close(v);   