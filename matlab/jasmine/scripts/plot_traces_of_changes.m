% this script plots the traces of changes in theta coherence for
% each electrodes during conditioning blocks, and shifts in centroid
% over time for each session

% define parameters
laser=[39 14];
laser_rec=[];

good_chl=good_chl_0627_session2;
date='20160627 Session 2 ';

cond_length=size(CondBlock1,1);
rec_length=size(RecBlock1,1);
good_coords=coords_xy(good_chl,:);

% define S1/M1 coordinates
S1_good=[];
for m = 1:length(good_chl)
    ind=find(S1==good_chl(m));
    S1_good=[S1_good S1(ind)];
end

M1_good=[];
for n = 1:length(good_chl)
    ind=find(M1==good_chl(n));
    M1_good=[M1_good M1(ind)];
end

S1_plot=S1_good;
M1_plot=M1_good;

% theta = 4-8Hz --> 9:17
% gamma = 30-60Hz --> 62:121
% high gamma = 60-200Hz --> 122:403
f_band=9:17;


%% absolute changes in theta coherence

adj_base = squeeze(mean(mean(RecBlock1(:,:,:,f_band),4)));
adj_base=adj_base(good_chl,good_chl);
adj_base=adj_base-eye(size(adj_base));

tot_abs_delta=[];
adj_abs_delta=0;
tot_center_shift=[];

alpha_coh=1;

for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock1(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    adj_abs_delta=abs(adj_delta);
    tot_abs_delta=[tot_abs_delta; sum(adj_abs_delta)];
    
    final_change_1=sum(adj_abs_delta);
    change_index=find(final_change_1>alpha_coh);
    change_coords=good_coords(change_index,:);
    change_coh=final_change_1(change_index);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        tot_center_shift=[tot_center_shift; center_shift];
    end
    old_center=center;
end


for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock2(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    adj_abs_delta=abs(adj_delta);
    tot_abs_delta=[tot_abs_delta; sum(adj_abs_delta)];
    
    final_change_2=sum(adj_abs_delta);
    change_index=find(final_change_2>alpha_coh);
    change_coords=good_coords(change_index,:);
    change_coh=final_change_2(change_index);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
    end
    old_center=center;
    tot_center_shift=[tot_center_shift; center_shift];
end


for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock3(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    adj_abs_delta=abs(adj_delta);
    tot_abs_delta=[tot_abs_delta; sum(adj_abs_delta)];
    
    final_change_3=sum(adj_abs_delta);
    change_index=find(final_change_3>alpha_coh);
    change_coords=good_coords(change_index,:);
    change_coh=final_change_3(change_index);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
    end
    old_center=center;
    tot_center_shift=[tot_center_shift; center_shift];
end


for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock4(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    adj_abs_delta=abs(adj_delta);
    tot_abs_delta=[tot_abs_delta; sum(adj_abs_delta)];
    
    final_change_4=sum(adj_abs_delta);
    change_index=find(final_change_4>alpha_coh);
    change_coords=good_coords(change_index,:);
    change_coh=final_change_4(change_index);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
    end
    old_center=center;
    tot_center_shift=[tot_center_shift; center_shift];
end


for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock5(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    adj_abs_delta=abs(adj_delta);
    tot_abs_delta=[tot_abs_delta; sum(adj_abs_delta)];
    
    final_change_5=sum(adj_abs_delta);
    change_index=find(final_change_5>alpha_coh);
    change_coords=good_coords(change_index,:);
    change_coh=final_change_5(change_index);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
    end
    old_center=center;
    tot_center_shift=[tot_center_shift; center_shift];
end


%{
for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock5(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=abs(adj_cond-adj_old);%get delta coherence
    adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    adj_abs_delta=adj_abs_delta+adj_delta;
    tot_abs_delta=[tot_abs_delta; sum(adj_abs_delta)];
  
    adj_old=adj_cond;
end
%}


time = linspace(0,50,length(tot_abs_delta));
figure;subplot(2,1,1)
plot(time,tot_abs_delta)
%ylim([-10 30]);
ylabel('Absolute \Delta Theta Coherence','FontSize',18)
xlabel('Total Conditioning Time (min)','FontSize',18)
title(join(['Total Absolute Change in Theta Coherence ',date]),'FontSize',18)
%

time1 = linspace(0,50,length(tot_center_shift));
subplot(2,1,2)
f=plot(time1,tot_center_shift);
f.LineWidth=2;

ylabel('Shift in center of change','FontSize',18)
xlabel('Total conditioning time (min)','FontSize',18)
title(join(['Shifts in centroid over time ',date]),'FontSize',18)
fig = gcf; % current figure handle
fig.Color = [1 1 1];
%}

%% raw change from baseline
%
adj_base = squeeze(mean(mean(RecBlock1(:,:,:,f_band),4)));
adj_base=adj_base(good_chl,good_chl);
adj_base=adj_base-eye(size(adj_base));

tot_delta=[];

for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock1(n,:,:,f_band),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    tot_delta=[tot_delta; sum(adj_delta)];
end

for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock2(n,:,:,f_band),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    tot_delta=[tot_delta; sum(adj_delta)];
end

for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock3(n,:,:,f_band),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    tot_delta=[tot_delta; sum(adj_delta)];
end

for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock4(n,:,:,f_band),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    tot_delta=[tot_delta; sum(adj_delta)];
end

for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock5(n,:,:,f_band),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    tot_delta=[tot_delta; sum(adj_delta)];
end
time = linspace(0,50,length(tot_delta));
subplot(3,1,1)
plot(time,tot_delta)
%ylim([-5 30]);
ylabel('\Delta Theta Coherence')
xlabel('Total Conditioning Time (min)')
title(join(['Total Change in Theta Coherence from Baseline ',date]))
fig = gcf; % current figure handle
fig.Color = [1 1 1];
%}

%% positive and negative changes
%
adj_base = squeeze(mean(RecBlock1(:,:,:,f_band),4));
adj_base=adj_base(good_chl,good_chl);
adj_base=adj_base-eye(size(adj_base));

center_shift1=0;
center_shift2=0;
tot_adj_pos=[];
tot_adj_neg=[];

tot_center_shift_pos=[];
tot_center_shift_neg=[];

alpha_coh=0.5;

for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock1(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    
    adj_pos=adj_delta;
    adj_pos(adj_pos<0)=0;
    tot_adj_pos=[tot_adj_pos; sum(adj_pos)];
    adj_neg=adj_delta;
    adj_neg(adj_neg>0)=0;
    tot_adj_neg=[tot_adj_neg; sum(adj_neg)];
    
    final_change_pos=sum(adj_pos);
    change_index_pos=find(final_change_pos>alpha_coh);
    change_coords=good_coords(change_index_pos,:);
    change_coh=final_change_pos(change_index_pos);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift1=sqrt((center(1)-old_center1(1))^2+(center(2)-old_center1(2))^2);
    end
    old_center1=center;
    tot_center_shift_pos=[tot_center_shift_pos; center_shift1];
    
    final_change_neg=sum(adj_neg);
    change_index_neg=find(final_change_neg<-alpha_coh);
    change_coords=good_coords(change_index_neg,:);
    change_coh=final_change_neg(change_index_neg);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift2=sqrt((center(1)-old_center2(1))^2+(center(2)-old_center2(2))^2);
    end
    old_center2=center;
    tot_center_shift_neg=[tot_center_shift_neg; center_shift2];
end


for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock2(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    
    adj_pos=adj_delta;
    adj_pos(adj_pos<0)=0;
    tot_adj_pos=[tot_adj_pos; sum(adj_pos)];
    adj_neg=adj_delta;
    adj_neg(adj_neg>0)=0;
    tot_adj_neg=[tot_adj_neg; sum(adj_neg)];
    
    final_change_pos=sum(adj_pos);
    change_index_pos=find(final_change_pos>alpha_coh);
    change_coords=good_coords(change_index_pos,:);
    change_coh=final_change_pos(change_index_pos);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift1=sqrt((center(1)-old_center1(1))^2+(center(2)-old_center1(2))^2);
    end
    old_center1=center;
    tot_center_shift_pos=[tot_center_shift_pos; center_shift1];
    
    final_change_neg=sum(adj_neg);
    change_index_neg=find(final_change_neg<-alpha_coh);
    change_coords=good_coords(change_index_neg,:);
    change_coh=final_change_neg(change_index_neg);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift2=sqrt((center(1)-old_center2(1))^2+(center(2)-old_center2(2))^2);
    end
    old_center2=center;
    tot_center_shift_neg=[tot_center_shift_neg; center_shift2];
end


for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock3(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    
    adj_pos=adj_delta;
    adj_pos(adj_pos<0)=0;
    tot_adj_pos=[tot_adj_pos; sum(adj_pos)];
    adj_neg=adj_delta;
    adj_neg(adj_neg>0)=0;
    tot_adj_neg=[tot_adj_neg; sum(adj_neg)];
    
    final_change_pos=sum(adj_pos);
    change_index_pos=find(final_change_pos>alpha_coh);
    change_coords=good_coords(change_index_pos,:);
    change_coh=final_change_pos(change_index_pos);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift1=sqrt((center(1)-old_center1(1))^2+(center(2)-old_center1(2))^2);
    end
    old_center1=center;
    tot_center_shift_pos=[tot_center_shift_pos; center_shift1];
    
    final_change_neg=sum(adj_neg);
    change_index_neg=find(final_change_neg<-alpha_coh);
    change_coords=good_coords(change_index_neg,:);
    change_coh=final_change_neg(change_index_neg);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift2=sqrt((center(1)-old_center2(1))^2+(center(2)-old_center2(2))^2);
    end
    old_center2=center;
    tot_center_shift_neg=[tot_center_shift_neg; center_shift2];
end


for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock4(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    
    adj_pos=adj_delta;
    adj_pos(adj_pos<0)=0;
    tot_adj_pos=[tot_adj_pos; sum(adj_pos)];
    adj_neg=adj_delta;
    adj_neg(adj_neg>0)=0;
    tot_adj_neg=[tot_adj_neg; sum(adj_neg)];
    
    final_change_pos=sum(adj_pos);
    change_index_pos=find(final_change_pos>alpha_coh);
    change_coords=good_coords(change_index_pos,:);
    change_coh=final_change_pos(change_index_pos);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift1=sqrt((center(1)-old_center1(1))^2+(center(2)-old_center1(2))^2);
    end
    old_center1=center;
    tot_center_shift_pos=[tot_center_shift_pos; center_shift1];
    
    final_change_neg=sum(adj_neg);
    change_index_neg=find(final_change_neg<-alpha_coh);
    change_coords=good_coords(change_index_neg,:);
    change_coh=final_change_neg(change_index_neg);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift2=sqrt((center(1)-old_center2(1))^2+(center(2)-old_center2(2))^2);
    end
    old_center2=center;
    tot_center_shift_neg=[tot_center_shift_neg; center_shift2];
end


for n=1:cond_length
    adj_cond=squeeze(mean(CondBlock5(n,:,:,9:17),4));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    
    adj_delta=adj_cond-adj_base;%get delta coherence
    
    adj_pos=adj_delta;
    adj_pos(adj_pos<0)=0;
    tot_adj_pos=[tot_adj_pos; sum(adj_pos)];
    adj_neg=adj_delta;
    adj_neg(adj_neg>0)=0;
    tot_adj_neg=[tot_adj_neg; sum(adj_neg)];
    
    final_change_pos=sum(adj_pos);
    change_index_pos=find(final_change_pos>alpha_coh);
    change_coords=good_coords(change_index_pos,:);
    change_coh=final_change_pos(change_index_pos);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift1=sqrt((center(1)-old_center1(1))^2+(center(2)-old_center1(2))^2);
    end
    old_center1=center;
    tot_center_shift_pos=[tot_center_shift_pos; center_shift1];
    
    final_change_neg=sum(adj_neg);
    change_index_neg=find(final_change_neg<-alpha_coh);
    change_coords=good_coords(change_index_neg,:);
    change_coh=final_change_neg(change_index_neg);
    center=find_cnter(change_coords, change_coh');
    if n~=1
        center_shift2=sqrt((center(1)-old_center2(1))^2+(center(2)-old_center2(2))^2);
    end
    old_center2=center;
    tot_center_shift_neg=[tot_center_shift_neg; center_shift2];
end
%
time = linspace(0,50,length(tot_center_shift_pos));

subplot(2,2,1)
plot(time,tot_adj_pos)
ylabel('Positive change in theta coherence','FontSize',18)
xlabel('Total Conditioning Time (min)','FontSize',18)
title(join(['Tota increase in theta coherence ',date]),'FontSize',18)


subplot(2,2,2)
plot(time,tot_adj_neg)
ylabel('Negative change in theta coherence','FontSize',18)
xlabel('Total Conditioning Time (min)','FontSize',18)
title(join(['Total decrease in theta coherence ',date]),'FontSize',18)


subplot(2,2,3)
plot(time,tot_center_shift_pos)
ylabel('Shift in center of positive change','FontSize',18)
xlabel('Total Conditioning Time (min)','FontSize',18)
title(join(['Positive change center ',date]),'FontSize',18)

subplot(2,2,4)
plot(time,tot_center_shift_neg)
ylabel('Shift in center of negative change','FontSize',18)
xlabel('Total Conditioning Time (min)','FontSize',18)
title(join(['Negative change center ',date]),'FontSize',18)
fig = gcf; % current figure handle
fig.Color = [1 1 1];
%}