function [hE,hV]=wgPlot_distance(adjMat,coord,large_coord,M1,S1,date,varargin)
%function [hE,hV]=wgPlot(adjMat,coord,varargin)
%
% Weighted Graph Plot from adjacency matrix [adjMat] and vertices
% coordinate [coord].
%
% INPUT:
%    [adjMat] = N x N sparse square adjacency matrix.
%     [coord] = N x 2 matrix of vertex coordinates of the graph to be plotted.
%  [varargin] = are specified as parameter value pairs where the parameters are
%     'edgeColorMap' = m1 x 3 edge colormap for coloring edges by their
%                      weight, {default=cool}
%        'edgeWidth' = scalar specifying the width of the edges. {default=0.1}
%     'vertexMarker' = single char, can be {.ox+*sdv^<>ph} as in plot {default='.'}
%   'vertexColorMap' = m2 x 3 vertex color map for coloring vertices by their
%                      weight, {default=summer}
%     'vertexWeight' = N x 1 vector of vertex weight that overrides using the
%                      diagonal of [adjMat] for specifying vertex size.
%      'vertexScale' = scalar vertex scaling factor for specifying scaling
%                      the size of vertices. {default=100}
%   'vertexmetadata' = N x 1 vector of vertex meta data for coloring the verticies.
% OUTPUT:
%  [hE] = vector/scalar of handles to edges of the drawn graph (1 per color).
%  [hV] = scalar handles to vertices of the drawn graph.
%
% SEE ALSO: gplot, treeplot, spy, plot
%
% By: Michael Wu  --  michael.wu@lithium.com (May 2009)
%
%====================


%% Set default parameter values
%--------------------
hold on; axis off;
h=gca; 
axesArea(h,[6 10 6 8]); %set [left, lower, right, upper] margins
plotParm={'markerSize',6,'lineWidth',0.1,'marker','.','MarkerEdgeColor',[1,0.5,0.2]};
%edgeMap=cool;


% Parse parameter value pairs
%--------------------
nVarArgin=length(varargin);
for kk=1:2:nVarArgin
    switch lower(varargin{kk})
        case 'edgecolormap'
            edgeMap=varargin{kk+1};
        case 'edgewidth'
            plotParm=[plotParm,{'lineWidth',varargin{kk+1}}];
        case 'vertexmarker'
            plotParm=[plotParm,{'marker',varargin{kk+1}}];
        case 'vertexcolormap'
            vrtxMap=varargin{kk+1};
        case 'vertexweight'
            vrtxWt=varargin{kk+1};
        case 'vertexmetadata'
            vrtxCol=varargin{kk+1};
        case 'vertexscale'
            vrtxSiz=varargin{kk+1};
        otherwise
            error(['wgPlot >< Unknown parameter ''',varargin{kk},'''.']) ;
    end
end


%% Find edge index
%--------------------
[ii,jj,eWt] = find(adjMat);% convert adjacency matrix to edge list
qq=unique([ii,jj]); %node index with edge???
minEWt=min(eWt);
maxEWt=max(eWt);
% minEWt=min_w;
% maxEWt=max_w;
% eWtRange=maxEWt-minEWt;
% eWeighted=eWtRange>0;


% Map edge weight to edge colormap

neColor=size(edgeMap,1);
% % normalize edge weight (acros all blocks) and match to colormap
eWt_map=ceil((neColor-1)*(eWt-minEWt)/(maxEWt-minEWt)+1);

% Fit to a blue white red colormap
%
% eWt_map=zeros(size(eWt));
% for n=1:length(eWt)
%     if eWt(n)>=0
%         eWt_map(n)=neColor/2+ceil(neColor/2*eWt(n)/maxEWt);
%     else
%         eWt_map(n)=neColor/2-ceil(neColor/2*eWt(n)/minEWt)+1;
%     end
%     
%     if eWt_map(n)>neColor
%         eWt_map(n)=neColor;
%     end
% end
%}
%% Plot weighted edges

hE=[];
%find the boundary of nodes with edges
maxX=max(coord(:,1));
minX=min(coord(:,1));
maxY=max(coord(:,2));
minY=min(coord(:,2));

legLim=linspace(minX,maxX,neColor+1);
yStep=(maxY-minY)/12; %empty space between data, legends, and label
legY=minY-yStep; %legend location

for kk=1:neColor
    p=find(eWt_map==kk);
    nSegment=length(p);
    x=[coord(ii(p),1),coord(jj(p),1),nan(nSegment,1)]';
    y=[coord(ii(p),2),coord(jj(p),2),nan(nSegment,1)]';
    hE=[hE,plot(x(:),y(:),'color',edgeMap(kk,:),plotParm{:})];

    % Draw legend bar
    line([legLim(kk),legLim(kk+1)],[legY,legY],'lineWidth',15,'color',edgeMap(kk,:));
end  % for kk

% Draw Legend Label
text([legLim(1),legLim(floor(end/2))+0.3,legLim(end)],...
    [legY-yStep,legY-yStep,legY-yStep], ...
    {'0','Distance Connectivity',num2str(maxEWt)},...
    'HorizontalAlignment','center','FontSize',18,'color',[0.2,0.2,0.2]);

%% plot electrodes at vertices

% scatter(coord(:,1),coord(:,2),100,'filled','MarkerFaceColor',[0.5,0.5,0.5]); 

% M1 in blue
scatter(large_coord(M1,1),large_coord(M1,2),80,'filled','MarkerFaceColor',[0.3,0.65,0.8])
% S1 in green
scatter(large_coord(S1,1),large_coord(S1,2),80,'filled','MarkerFaceColor',[0.3,0.6,0.4])


%% axis and title
title(join([date,'Structural Connectivity']),'FontSize',18)
axis tight;
ax=axis;
dxRange=(ax(2)-ax(1))/500;
dyRange=(ax(4)-ax(3))/500;
axis([ax(1)-dxRange,ax(2)+dxRange,ax(3)-dyRange,ax(4)+dyRange]);
hold off

function [hAx]=axesArea(varargin)
%function [hAx]=axesArea(hAx,varargin)
%
% Set the margine of the axis by specifying a vector of distance to the figure edges.
%
% Input:
%  [varargin=hAx] = axis handle
%    [varargin=p] = position spec: 1, 2 or 4 element vector that specify the distance 
%                   from the edges of the figure by a percentage number between (0-49). 
%                   If 1 element it is used for all margins. 
%                   If 2 elements, p=[x-margins, y-margins].
%                   If 4 elements, p=[left, lower, right, upper] margins.
% Output:
%  [hAx] = axis handle of the axes.
%
% See also: axis, axes, ishandle, set
%
% By: Michael Wu  --  michael.wu@lithium.com (Mar 2009)
%
%====================


% Check if 1st input is axis handle
%--------------------
if ishghandle(varargin{1},'axes')
	hAx=varargin{1};
	p=varargin{2};
else
	hAx=gca;
	p=varargin{1};
end


% Process input arguments
%--------------------
p(p<0)=0;
p(p>49)=49;
p=p/100;


% Compute position property to be set
%--------------------
switch length(p)
	case 1
		xmin=p;
		ymin=xmin;
		xlen=1-2*p;
		ylen=xlen;
	case 2
		xmin=p(1);
		ymin=p(2);
		xlen=1-2*p(1);
		ylen=1-2*p(2);
	case 4
		xmin=p(1);
		ymin=p(2);
		xlen=1-p(1)-p(3);
		ylen=1-p(2)-p(4);	
	otherwise
		% Default Matlab position setting
		%--------------------
		xmin=0.13;
		ymin=0.11;
		xlen=0.775;
		ylen=0.815;	
end


% Set new position property of the axes
%--------------------
set(hAx,'position',[xmin ymin xlen ylen]);
