% plot structural connectivity graph based on distance

good_chl=good_chl_0908;
good_coords=coords_xy(good_chl,:);
d_adj = distance_adj(coords_xy,good_chl,S1,M1);

S1_good=[];
for m = 1:length(good_chl)
    ind=find(S1==good_chl(m));
    S1_good=[S1_good S1(ind)];
end

M1_good=[];
for n = 1:length(good_chl)
    ind=find(M1==good_chl(n));
    M1_good=[M1_good M1(ind)];
end

date = '20160908 ';
figure;
wgPlot_distance(d_adj,good_coords,coords_xy,M1_good,S1_good,date,'edgeColorMap',map,'edgeWidth',1);
fig=gcf; fig.Color = [1 1 1];
img = getframe(fig);
imwrite(img.cdata, [join([date,'Structural']),'.png']);


