function d_adj = distance_adj(all_coords,good_chl,S1,M1)
d_adj=zeros(96,96);
for m = 1:length(all_coords)
    for n = 1:length(all_coords)
        D = sqrt((all_coords(m,1)-all_coords(n,1))^2+(all_coords(m,2)-all_coords(n,2))^2);
        if isempty(find(M1==m,1)) && isempty(find(S1==n,1))
            con=1/D/5;
        elseif isempty(find(M1==n,1)) && isempty(find(S1==m,1))
            con=1/D/5;
        elseif D==0
            con=0;
        else
            con=1/D;
        end
        d_adj(m,n)=con;
    end
end

d_adj=d_adj(good_chl,good_chl);
end