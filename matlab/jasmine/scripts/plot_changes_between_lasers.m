% this script plots the change in theta coherence between locations
% of laser stimulation, during recording blocks

% define parameters
laser=[39 14];
laser_rec=[];

good_chl=good_chl_0627_session2;
date='20160627 Session 2 ';

cond_length=size(CondBlock1,1);
rec_length=size(RecBlock1,1);
good_coords=coords_xy(good_chl,:);

% define S1/M1 coordinates
S1_good=[];
for m = 1:length(good_chl)
    ind=find(S1==good_chl(m));
    S1_good=[S1_good S1(ind)];
end

M1_good=[];
for n = 1:length(good_chl)
    ind=find(M1==good_chl(n));
    M1_good=[M1_good M1(ind)];
end

S1_plot=S1_good;
M1_plot=M1_good;

% theta = 4-8Hz --> 9:17
% gamma = 30-60Hz --> 62:121
% high gamma = 60-200Hz --> 122:403
f_band=9:17;

%% plot changes between lasers
% during recording blocks
%
adj_laser_1=[];
for n=1:rec_length
    adj_rec=squeeze(mean(RecBlock1(n,:,:,f_band),4));
    adj_laser=adj_rec(laser(1),laser(2));
 
    adj_laser_1=[adj_laser_1; adj_laser];
end
figure;
hold on
time = 20:20:300;
f=plot(time,adj_laser_1,'k');
f.LineWidth=2;

adj_laser_2=[];
for n=1:rec_length
    adj_rec=squeeze(mean(RecBlock2(n,:,:,f_band),4));
    adj_laser=adj_rec(laser(1),laser(2));
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    adj_laser_2=[adj_laser_2; adj_laser];
end
time = 320:20:600;
f=plot(time,adj_laser_2,'k');
f.LineWidth=2;

adj_laser_3=[];
for n=1:rec_length
    adj_rec=squeeze(mean(RecBlock3(n,:,:,f_band),4));
    adj_laser=adj_rec(laser(1),laser(2));
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    adj_laser_3=[adj_laser_3; adj_laser];
end
time = 620:20:900;
f=plot(time,adj_laser_3,'k');
f.LineWidth=2;

adj_laser_4=[];
for n=1:rec_length
    adj_rec=squeeze(mean(RecBlock4(n,:,:,f_band),4));
    adj_laser=adj_rec(laser(1),laser(2));
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    adj_laser_4=[adj_laser_4; adj_laser];
end
time = 920:20:1200;
f=plot(time,adj_laser_4,'k');
f.LineWidth=2;

adj_laser_5=[];
for n=1:rec_length
    adj_rec=squeeze(mean(RecBlock5(n,:,:,f_band),4));
    adj_laser=adj_rec(laser(1),laser(2));
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    adj_laser_5=[adj_laser_5; adj_laser];
end
time = 1220:20:1500;
f=plot(time,adj_laser_5,'k');
f.LineWidth=2;

adj_laser_6=[];
for n=1:rec_length
    adj_rec=squeeze(mean(RecBlock6(n,:,:,f_band),4));
    adj_laser=adj_rec(laser(1),laser(2));
    %adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
    adj_laser_6=[adj_laser_6; adj_laser];
end
time = 1520:20:1800;
f=plot(time,adj_laser_6,'k');
f.LineWidth=2;

max_laser=1;
% max_laser=1.1*max(max([adj_laser_1 adj_laser_2 adj_laser_3...
%     adj_laser_4 adj_laser_5 adj_laser_6]));

f1=area([0 300],[max_laser max_laser],'LineStyle','none');
f1.FaceAlpha = 0.3;
f1.FaceColor = [0.2327 0.4805 0.4461];

f2=area([300 600],[max_laser max_laser],'LineStyle','none');
f2.FaceAlpha = 0.3;
f2.FaceColor = [0.6186 0.7353 1.0000];

f3=area([600 900],[max_laser max_laser],'LineStyle','none');
f3.FaceAlpha = 0.3;
f3.FaceColor = [0.3040 0.5914 0.6491];

f4=area([900 1200],[max_laser max_laser],'LineStyle','none');
f4.FaceAlpha = 0.3;
f4.FaceColor = [0.8147 0.8034 1.0000];

f5=area([1200 1500],[max_laser max_laser],'LineStyle','none');
f5.FaceAlpha = 0.3;
f5.FaceColor = [0.4377 0.6714 0.8481];

f6=area([1500 1800],[max_laser max_laser],'LineStyle','none');
f6.FaceAlpha = 0.3;
f6.FaceColor = [0.9864 0.8951 1.0000];

ylabel('Theta Coherence')
xlabel('Total Recording Time (sec)')
title(join(['Baseline Theta Coherence b/w Lasers ',date]))
fig = gcf; % current figure handle
fig.Color = [1 1 1];
hold off
%}
