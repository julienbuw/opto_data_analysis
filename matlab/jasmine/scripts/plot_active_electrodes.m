% this script plots the most active electrodes (changes above threshold)
% for each conditioning blocks

laser=[39 14];
laser_rec=[];

good_chl=good_chl_0627_session2;
date='20160627 Session 2 ';

cond_length=size(CondBlock1,1);
rec_length=size(RecBlock1,1);
good_coords=coords_xy(good_chl,:);

% define S1/M1 coordinates
S1_good=[];
for m = 1:length(good_chl)
    ind=find(S1==good_chl(m));
    S1_good=[S1_good S1(ind)];
end

M1_good=[];
for n = 1:length(good_chl)
    ind=find(M1==good_chl(n));
    M1_good=[M1_good M1(ind)];
end

S1_plot=S1_good;
M1_plot=M1_good;

% theta = 4-8Hz --> 9:17
% gamma = 30-60Hz --> 62:121
% high gamma = 60-200Hz --> 122:403
f_band=9:17;

%% find greatest absolute changes and centroids for conditioning snapshots
%
alpha_coh=2.4;

adj_base = squeeze(mean(mean(RecBlock1(:,:,:,f_band),4)));
adj_base=adj_base(good_chl,good_chl);
adj_base=adj_base-eye(size(adj_base));

%CondBlock1
adj_cond=squeeze(mean(mean(CondBlock1(:,:,:,9:17),4)));
adj_cond=adj_cond(good_chl,good_chl);
adj_cond=adj_cond-eye(size(adj_cond));
adj_delta=adj_cond-adj_base;%get delta coherence
adj_abs_delta=abs(adj_delta);
final_change_1=sum(adj_abs_delta);
change_index=find(final_change_1>alpha_coh);
figure;fig = gcf; % current figure handle
fig.Color = [1 1 1];
thresh_adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
wgPlot_jz(thresh_adj_delta,good_coords,coords_xy,change_index,M1_plot,S1_plot,1,-1,laser,date,'CondBlock1','edgeColorMap',c,'edgeWidth',1.6);
hold on
change_coords=good_coords(change_index,:);
change_coh=final_change_1(change_index);
[center_coord] = find_cnter(change_coords, change_coh');
scatter(center_coord(1),center_coord(2),400,'p','filled','MarkerFaceColor',[1 1 0.1])

% separate positive and negative changes
%{
adj_pos=adj_delta;
adj_pos(adj_pos<0)=0;
tot_adj_pos=[tot_adj_pos; sum(adj_pos)];
adj_neg=adj_delta;
adj_neg(adj_neg>0)=0;
tot_adj_neg=[tot_adj_neg; sum(adj_neg)];
    
final_change_pos=sum(adj_pos);
change_index_pos=find(final_change_pos>alpha_coh);
change_coords=good_coords(change_index_pos,:);
change_coh=final_change_pos(change_index_pos);
center_pos=find_cnter(change_coords, change_coh');
scatter(center_pos(1),center_pos(2),400,'p','filled','MarkerFaceColor',[1 0 0])

final_change_neg=sum(adj_neg);
change_index_neg=find(final_change_neg<-alpha_coh);
change_coords=good_coords(change_index_neg,:);
change_coh=final_change_neg(change_index_neg);
center_neg=find_cnter(change_coords, change_coh');
scatter(center_neg(1),center_neg(2),400,'p','filled','MarkerFaceColor',[0 0 1])
%}

%CondBlock2
adj_cond=squeeze(mean(mean(CondBlock2(:,:,:,9:17),4)));
adj_cond=adj_cond(good_chl,good_chl);
adj_cond=adj_cond-eye(size(adj_cond));
adj_delta=adj_cond-adj_base;%get delta coherence
adj_abs_delta=abs(adj_delta);
final_change_2=sum(adj_abs_delta);
change_index=find(final_change_2>alpha_coh);
figure;fig = gcf; % current figure handle
fig.Color = [1 1 1];
thresh_adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
wgPlot_jz(thresh_adj_delta,good_coords,coords_xy,change_index,M1_plot,S1_plot,1,-1,laser,date,'CondBlock2','edgeColorMap',c,'edgeWidth',1.6);
hold on
change_coords=good_coords(change_index,:);
change_coh=final_change_2(change_index);
[center_coord] = find_cnter(change_coords, change_coh');
scatter(center_coord(1),center_coord(2),400,'p','filled','MarkerFaceColor',[1 1 0.1])

% positive and negative changes
%{
adj_pos=adj_delta;
adj_pos(adj_pos<0)=0;
tot_adj_pos=[tot_adj_pos; sum(adj_pos)];
adj_neg=adj_delta;
adj_neg(adj_neg>0)=0;
tot_adj_neg=[tot_adj_neg; sum(adj_neg)];
    
final_change_pos=sum(adj_pos);
change_index_pos=find(final_change_pos>alpha_coh);
change_coords=good_coords(change_index_pos,:);
change_coh=final_change_pos(change_index_pos);
center_pos=find_cnter(change_coords, change_coh');
scatter(center_pos(1),center_pos(2),400,'p','filled','MarkerFaceColor',[1 0 0])

final_change_neg=sum(adj_neg);
change_index_neg=find(final_change_neg<-alpha_coh);
change_coords=good_coords(change_index_neg,:);
change_coh=final_change_neg(change_index_neg);
center_neg=find_cnter(change_coords, change_coh');
scatter(center_neg(1),center_neg(2),400,'p','filled','MarkerFaceColor',[0 0 1])
%}

%CondBlock3
adj_cond=squeeze(mean(mean(CondBlock3(:,:,:,9:17),4)));
adj_cond=adj_cond(good_chl,good_chl);
adj_cond=adj_cond-eye(size(adj_cond));
adj_delta=adj_cond-adj_base;%get delta coherence
adj_abs_delta=abs(adj_delta);
final_change_3=sum(adj_abs_delta);
change_index=find(final_change_3>alpha_coh);
figure;fig = gcf; % current figure handle
fig.Color = [1 1 1];
thresh_adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
wgPlot_jz(thresh_adj_delta,good_coords,coords_xy,change_index,M1_plot,S1_plot,1,-1,laser,date,'CondBlock3','edgeColorMap',c,'edgeWidth',1.6);
hold on
change_coords=good_coords(change_index,:);
change_coh=final_change_3(change_index);
[center_coord] = find_cnter(change_coords, change_coh');
scatter(center_coord(1),center_coord(2),400,'p','filled','MarkerFaceColor',[1 1 0.1])

% positive and negative changes
%{
adj_pos=adj_delta;
adj_pos(adj_pos<0)=0;
tot_adj_pos=[tot_adj_pos; sum(adj_pos)];
adj_neg=adj_delta;
adj_neg(adj_neg>0)=0;
tot_adj_neg=[tot_adj_neg; sum(adj_neg)];
    
final_change_pos=sum(adj_pos);
change_index_pos=find(final_change_pos>alpha_coh);
change_coords=good_coords(change_index_pos,:);
change_coh=final_change_pos(change_index_pos);
center_pos=find_cnter(change_coords, change_coh');
scatter(center_pos(1),center_pos(2),400,'p','filled','MarkerFaceColor',[1 0 0])

final_change_neg=sum(adj_neg);
change_index_neg=find(final_change_neg<-alpha_coh);
change_coords=good_coords(change_index_neg,:);
change_coh=final_change_neg(change_index_neg);
center_neg=find_cnter(change_coords, change_coh');
scatter(center_neg(1),center_neg(2),400,'p','filled','MarkerFaceColor',[0 0 1])
%}

%CondBlock4
adj_cond=squeeze(mean(mean(CondBlock4(:,:,:,9:17),4)));
adj_cond=adj_cond(good_chl,good_chl);
adj_cond=adj_cond-eye(size(adj_cond));
adj_delta=adj_cond-adj_base;%get delta coherence
adj_abs_delta=abs(adj_delta);
final_change_4=sum(adj_abs_delta);
change_index=find(final_change_4>alpha_coh);
figure;fig = gcf; % current figure handle
fig.Color = [1 1 1];
thresh_adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
wgPlot_jz(thresh_adj_delta,good_coords,coords_xy,change_index,M1_plot,S1_plot,1,-1,laser,date,'CondBlock4','edgeColorMap',c,'edgeWidth',1.6);
hold on
change_coords=good_coords(change_index,:);
change_coh=final_change_4(change_index);
[center_coord] = find_cnter(change_coords, change_coh');
scatter(center_coord(1),center_coord(2),400,'p','filled','MarkerFaceColor',[1 1 0.1])

% positive and negative changes
%{
adj_pos=adj_delta;
adj_pos(adj_pos<0)=0;
tot_adj_pos=[tot_adj_pos; sum(adj_pos)];
adj_neg=adj_delta;
adj_neg(adj_neg>0)=0;
tot_adj_neg=[tot_adj_neg; sum(adj_neg)];
    
final_change_pos=sum(adj_pos);
change_index_pos=find(final_change_pos>alpha_coh);
change_coords=good_coords(change_index_pos,:);
change_coh=final_change_pos(change_index_pos);
center_pos=find_cnter(change_coords, change_coh');
scatter(center_pos(1),center_pos(2),400,'p','filled','MarkerFaceColor',[1 0 0])

final_change_neg=sum(adj_neg);
change_index_neg=find(final_change_neg<-alpha_coh);
change_coords=good_coords(change_index_neg,:);
change_coh=final_change_neg(change_index_neg);
center_neg=find_cnter(change_coords, change_coh');
scatter(center_neg(1),center_neg(2),400,'p','filled','MarkerFaceColor',[0 0 1])
%}
%CondBlock5
adj_cond=squeeze(mean(mean(CondBlock5(:,:,:,9:17),4)));
adj_cond=adj_cond(good_chl,good_chl);
adj_cond=adj_cond-eye(size(adj_cond));
adj_delta=adj_cond-adj_base;%get delta coherence
adj_abs_delta=abs(adj_delta);
final_change_5=sum(adj_abs_delta);
change_index=find(final_change_5>alpha_coh);
figure;fig = gcf; % current figure handle
fig.Color = [1 1 1];
thresh_adj_delta=threshold_adj(adj_delta,good_coords,0.05,6);
wgPlot_jz(thresh_adj_delta,good_coords,coords_xy,change_index,M1_plot,S1_plot,1,-1,laser,date,'CondBlock5','edgeColorMap',c,'edgeWidth',1.6);
hold on
change_coords=good_coords(change_index,:);
change_coh=final_change_5(change_index);
[center_coord] = find_cnter(change_coords, change_coh');
scatter(center_coord(1),center_coord(2),400,'p','filled','MarkerFaceColor',[1 1 0.1])

% positive and negative changes
%{
adj_pos=adj_delta;
adj_pos(adj_pos<0)=0;
tot_adj_pos=[tot_adj_pos; sum(adj_pos)];
adj_neg=adj_delta;
adj_neg(adj_neg>0)=0;
tot_adj_neg=[tot_adj_neg; sum(adj_neg)];
    
final_change_pos=sum(adj_pos);
change_index_pos=find(final_change_pos>alpha_coh);
change_coords=good_coords(change_index_pos,:);
change_coh=final_change_pos(change_index_pos);
center_pos=find_cnter(change_coords, change_coh');
scatter(center_pos(1),center_pos(2),400,'p','filled','MarkerFaceColor',[1 0 0])

final_change_neg=sum(adj_neg);
change_index_neg=find(final_change_neg<-alpha_coh);
change_coords=good_coords(change_index_neg,:);
change_coh=final_change_neg(change_index_neg);
center_neg=find_cnter(change_coords, change_coh');
scatter(center_neg(1),center_neg(2),400,'p','filled','MarkerFaceColor',[0 0 1])
%}
%}

