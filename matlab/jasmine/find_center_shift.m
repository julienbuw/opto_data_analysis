function find_center_shift
% this function calculates the shift in centroid of change in 
% theta coherence from baseline for all active electrodes
% then plots the results for single laser, long delay, short delay
% and control sessions

load('sorted_data_J.mat','single_laser','long_delay','short_delay','control');
load('coords_xy.mat','coords_xy')

% set threshold
c1=0.12;c2=1;

% set #sessions for each stimulation pattern 
num_single = length(single_laser(:,1));
num_long = length(long_delay(:,1));
num_short = length(short_delay(:,1));
num_control = length(control(:,1));

%% single laser sessions

% initialize shift matrix
tot_center_shift=zeros(num_single,5);
cent_shift_all=zeros(5,num_single,29);

for n = 1:num_single
    %set good channels
    good_chl=single_laser{n,3};
    %set good coords
    good_coords=coords_xy(good_chl,:);
    %set recording blocks
    rec_data=single_laser{n,1};
    %set conditioning blocks
    cond_data=single_laser{n,2};
    cond_length=length(cond_data(:,1,1,1));
    
    % calculate average coh for baseline
    if ndims(rec_data)==3
        adj_base = rec_data(:,:,1);
    else
        adj_base = squeeze(mean(rec_data(:,:,:,1)));
    end
    adj_base=adj_base(good_chl,good_chl);
    adj_base=adj_base-eye(size(adj_base));
    % set thresholding method 1
    alpha_coh_1=sum(sum(adj_base))/length(good_chl)*c1;

    % find average change for condblock1
    first_cond=squeeze(mean(cond_data(:,:,:,1)));
    first_cond=first_cond(good_chl,good_chl);
    first_cond=first_cond-eye(size(first_cond));
    adj_delta=first_cond-adj_base;%get delta coherence
    abs_delta=sum(abs(adj_delta));
    % set thresholding method 2
    alpha_coh_2=sum(abs_delta)/length(good_chl)*c2;
    
    % CondBlock1
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,1));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,1)=sum(center_shift)/length(center_shift);
    cent_shift_all(1,n,:)=center_shift;
    
    % CondBlock2
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,2));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,2)=sum(center_shift)/length(center_shift);
    cent_shift_all(2,n,:)=center_shift;
    
    % CondBlock3
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,3));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,3)=sum(center_shift)/length(center_shift);
    cent_shift_all(3,n,:)=center_shift;
    
    % CondBlock4
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,4));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,4)=sum(center_shift)/length(center_shift);
    cent_shift_all(4,n,:)=center_shift;
    
    % CondBlock5
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,5));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,5)=sum(center_shift)/length(center_shift);
    cent_shift_all(5,n,:)=center_shift;
end
% calculate mean and standard error
average_shift_single=mean(tot_center_shift)';
sem_single=std(tot_center_shift).'/sqrt(num_single); 
% normalize using condblock1
total_ratio_single=[tot_center_shift(:,2)./tot_center_shift(:,1) ...
    tot_center_shift(:,3)./tot_center_shift(:,1) ...
    tot_center_shift(:,4)./tot_center_shift(:,1) ...
    tot_center_shift(:,5)./tot_center_shift(:,1)];
% calculate normalized mean and standard error
average_ratio_single=mean(total_ratio_single)';
sem_ratio_single=std(total_ratio_single).'/sqrt(num_single);

% store all frames across sessions into a single vector
shift_single_block1=squeeze(cent_shift_all(1,:,:));
all_shift_single_block1=shift_single_block1(:);
shift_single_block5=squeeze(cent_shift_all(5,:,:));
all_shift_single_block5=shift_single_block5(:);

%% long delay sessions
tot_center_shift=zeros(num_long,5);
cent_shift_all=zeros(5,num_long,29);

for n = 1:num_long
    %set good channels
    good_chl=long_delay{n,3};
    %set good coords
    good_coords=coords_xy(good_chl,:);
    %set recording blocks
    rec_data=long_delay{n,1};
    %set conditioning blocks
    cond_data=long_delay{n,2};
    cond_length=length(cond_data(:,1,1,1));
    
    % calculate average coh for baseline
    if ndims(rec_data)==3
        adj_base = rec_data(:,:,1);
    else
        adj_base = squeeze(mean(rec_data(:,:,:,1)));
    end
    adj_base=adj_base(good_chl,good_chl);
    adj_base=adj_base-eye(size(adj_base));
    alpha_coh_1=sum(sum(adj_base))/length(good_chl)*c1;
    
    % find average change for condblock1
    first_cond=squeeze(mean(cond_data(:,:,:,1)));
    first_cond=first_cond(good_chl,good_chl);
    first_cond=first_cond-eye(size(first_cond));
    adj_delta=first_cond-adj_base;%get delta coherence
    abs_delta=sum(abs(adj_delta));
    alpha_coh_2=sum(abs_delta)/length(good_chl)*c2;
    
    % CondBlock1
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,1));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,1)=sum(center_shift)/length(center_shift);
    cent_shift_all(1,n,:)=center_shift;
    
    % CondBlock2
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,2));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,2)=sum(center_shift)/length(center_shift);
    cent_shift_all(2,n,:)=center_shift;
    
    % CondBlock3
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,3));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,3)=sum(center_shift)/length(center_shift);
    cent_shift_all(3,n,:)=center_shift;
    
    % CondBlock4
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,4));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,4)=sum(center_shift)/length(center_shift);
    cent_shift_all(4,n,:)=center_shift;
    
    % CondBlock5
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,5));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,5)=sum(center_shift)/length(center_shift);
    cent_shift_all(5,n,:)=center_shift;
end
% calculate mean and standard errors
average_shift_long=mean(tot_center_shift)';
sem_long=std(tot_center_shift).'/sqrt(num_long);
% normalize using condblock1
total_ratio_long=[tot_center_shift(:,2)./tot_center_shift(:,1) ...
    tot_center_shift(:,3)./tot_center_shift(:,1) ...
    tot_center_shift(:,4)./tot_center_shift(:,1) ...
    tot_center_shift(:,5)./tot_center_shift(:,1)];
average_ratio_long=mean(total_ratio_long)';
sem_ratio_long=std(total_ratio_long).'/sqrt(num_long);

% store all frames across sessions into a single vector
shift_long_block1=squeeze(cent_shift_all(1,:,:));
all_shift_long_block1=shift_long_block1(:);
shift_long_block5=squeeze(cent_shift_all(5,:,:));
all_shift_long_block5=shift_long_block5(:);

%% short delay sessions
tot_center_shift=zeros(num_short,5);
cent_shift_all=zeros(5,num_short,29);

for n = 1:num_short
    %set good channels
    good_chl=short_delay{n,3};
    %set good coords
    good_coords=coords_xy(good_chl,:);
    %set recording blocks
    rec_data=short_delay{n,1};
    %set conditioning blocks
    cond_data=short_delay{n,2};
    cond_length=length(cond_data(:,1,1,1));
    
    % calculate average coh for baseline
    if ndims(rec_data)==3
        adj_base = rec_data(:,:,1);
    else
        adj_base = squeeze(mean(rec_data(:,:,:,1)));
    end
    adj_base=adj_base(good_chl,good_chl);
    adj_base=adj_base-eye(size(adj_base));
    % threholding method 1
    alpha_coh_1=sum(sum(adj_base))/length(good_chl)*c1;

    % find average change for condblock1
    first_cond=squeeze(mean(cond_data(:,:,:,1)));
    first_cond=first_cond(good_chl,good_chl);
    first_cond=first_cond-eye(size(first_cond));
    adj_delta=first_cond-adj_base;%get delta coherence
    abs_delta=sum(abs(adj_delta));
    % thresholding method 2
    alpha_coh_2=sum(abs_delta)/length(good_chl)*c2;
    
    % CondBlock1
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,1));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,1)=sum(center_shift)/length(center_shift);
    cent_shift_all(1,n,:)=center_shift;
    
    % CondBlock2
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,2));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,2)=sum(center_shift)/length(center_shift);
    cent_shift_all(2,n,:)=center_shift;
    
    % CondBlock3
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,3));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,3)=sum(center_shift)/length(center_shift);
    cent_shift_all(3,n,:)=center_shift;
    
    % CondBlock4
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,4));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,4)=sum(center_shift)/length(center_shift);
    cent_shift_all(4,n,:)=center_shift;
    
    % CondBlock5
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,5));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,5)=sum(center_shift)/length(center_shift);
    cent_shift_all(5,n,:)=center_shift;
end
% calculate mean and standard errors
average_shift_short=mean(tot_center_shift)';
sem_short=std(tot_center_shift).'/sqrt(num_short);
% normalize using condblock1
total_ratio_short=[tot_center_shift(:,2)./tot_center_shift(:,1) ...
    tot_center_shift(:,3)./tot_center_shift(:,1) ...
    tot_center_shift(:,4)./tot_center_shift(:,1) ...
    tot_center_shift(:,5)./tot_center_shift(:,1)];
average_ratio_short=mean(total_ratio_short)';
sem_ratio_short=std(total_ratio_short).'/sqrt(num_short);

% store all frames across sessions into a single vector
shift_short_block1=squeeze(cent_shift_all(1,:,:));
all_shift_short_block1=shift_short_block1(:);
shift_short_block5=squeeze(cent_shift_all(5,:,:));
all_shift_short_block5=shift_short_block5(:);

%% control sessions
tot_center_shift=zeros(num_control,5);
cent_shift_all=zeros(5,num_control,29);
for n = 1:num_control
    %set good channels
    good_chl=control{n,3};
    %set good coords
    good_coords=coords_xy(good_chl,:);
    %set recording blocks
    rec_data=control{n,1};
    %set conditioning blocks
    cond_data=control{n,2};
    cond_length=length(cond_data(:,1,1,1));
    
    % calculate average coh for baseline
    if ndims(rec_data)==3
        adj_base = rec_data(:,:,1);
    else
        adj_base = squeeze(mean(rec_data(:,:,:,1)));
    end
    adj_base=adj_base(good_chl,good_chl);
    adj_base=adj_base-eye(size(adj_base));
    alpha_coh_1=sum(sum(adj_base))/length(good_chl)*c1;
    % find average change for condblock1
    first_cond=squeeze(mean(cond_data(:,:,:,1)));
    first_cond=first_cond(good_chl,good_chl);
    first_cond=first_cond-eye(size(first_cond));
    adj_delta=first_cond-adj_base;%get delta coherence
    abs_delta=sum(abs(adj_delta));
    alpha_coh_2=sum(abs_delta)/length(good_chl)*c2;
    
    % CondBlock1
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,1));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,1)=sum(center_shift)/length(center_shift);
    cent_shift_all(1,n,:)=center_shift;
    
    % CondBlock2
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,2));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,2)=sum(center_shift)/length(center_shift);
    cent_shift_all(2,n,:)=center_shift;
    
    % CondBlock3
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,3));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,3)=sum(center_shift)/length(center_shift);
    cent_shift_all(3,n,:)=center_shift;
    
    % CondBlock4
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,4));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,4)=sum(center_shift)/length(center_shift);
    cent_shift_all(4,n,:)=center_shift;
    
    % CondBlock5
    center_shift=zeros(cond_length-1,1);
    for ii=1:cond_length
        adj_cond=squeeze(cond_data(ii,:,:,5));
        adj_cond=adj_cond(good_chl,good_chl);
        adj_cond=adj_cond-eye(size(adj_cond));
        adj_delta=adj_cond-adj_base;%get delta coherence
        adj_abs_delta=abs(adj_delta);
        final_change=sum(adj_abs_delta);
        change_index=find(final_change>alpha_coh_1);
        change_coords=good_coords(change_index,:);
        change_coh=final_change(change_index);
        center=find_center(change_coords, change_coh');
        if ii~=1
            center_shift(ii-1)=sqrt((center(1)-old_center(1))^2+(center(2)-old_center(2))^2);
        end
        old_center=center;
    end
    tot_center_shift(n,5)=sum(center_shift)/length(center_shift);
    cent_shift_all(5,n,:)=center_shift;
end
% calculate mean and standard errors
average_shift_control=mean(tot_center_shift)';
sem_control=std(tot_center_shift).'/sqrt(num_control);
% normalize using condblock1
total_ratio_control=[tot_center_shift(:,2)./tot_center_shift(:,1) ...
    tot_center_shift(:,3)./tot_center_shift(:,1) ...
    tot_center_shift(:,4)./tot_center_shift(:,1) ...
    tot_center_shift(:,5)./tot_center_shift(:,1)];
average_ratio_control=mean(total_ratio_control)';
sem_ratio_control=std(total_ratio_control).'/sqrt(num_control);

% store all frames across sessions into a single vector
shift_control_block1=squeeze(cent_shift_all(1,:,:));
all_shift_control_block1=shift_control_block1(:);
shift_control_block5=squeeze(cent_shift_all(5,:,:));
all_shift_control_block5=shift_control_block5(:);

%% plot results for each conditioning block
figure;
X=[1 2 3 4 5];
Y=[average_shift_control average_shift_single average_shift_long average_shift_short];
err=[sem_control sem_single sem_long sem_short];
bar(X,Y,'FaceAlpha',0.6);
ylabel('Average shift in center of change between 20s windows')
ylim([0 1.5]);

set(gca, 'XTickLabel', {'CondBlock1','CondBlock2','CondBlock3','CondBlock4','CondBlock5'});
hold on
ngroups = size(Y, 1);
nbars = size(Y, 2);
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    ebar=errorbar(x, Y(:,i), err(:,i), '.');
    ebar.Color = [0 0 0];
    ebar.LineWidth = 1.5;
end
legend('control','single laser','long delay','short delay','Location','northwest')
fig = gcf; % current figure handle
fig.Color = [1 1 1];
hold off
title('Shifts in center of change during conditioning blocks')

%% plot normalized results

X=[1 2 3 4];
Y=[average_ratio_control average_ratio_single average_ratio_long average_ratio_short];
err=[sem_ratio_control sem_ratio_single sem_ratio_long sem_ratio_short];
figure;b2=bar(X,Y,'FaceAlpha',0.6);
set(gca, 'XTickLabel', {'#block2/1','#block3/1','#block4/1','#block5/1'});
hold on
ngroups = size(Y, 1);
nbars = size(Y, 2);
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    ebar=errorbar(x, Y(:,i), err(:,i), '.');
    ebar.Color = [0 0 0];
    ebar.LineWidth = 1.5;
end
legend('control','single laser','long delay','short delay','Location','northwest')
fig = gcf; % current figure handle
fig.Color = [1 1 1];
hold off
title('Normalized shifts in center of change using CondBlock1')
end


%%
function [center_coord] = find_center(coords, delta_coh)
% this function finds the centroid of electrodes with given coordinates 
% and change is theta coherence from baseline
center_x=sum(coords(:,1).*delta_coh)/sum(delta_coh);
center_y=sum(coords(:,2).*delta_coh)/sum(delta_coh);
center_coord=[center_x center_y];
end
