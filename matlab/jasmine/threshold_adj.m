function thresh_adj = threshold_adj(Adj,Coord,Coh,d)
% modify the adjacency matrix by thresholding coherence and distance
% Input
%   [Adj] = NxN original adjacency matrix (with only good channels)
%   [Coord] = Nx2 coordinates of the good electrodes
%   [Coh] = threshold coherence between two electrodes
%   [d] = threshold distance between two electrodes
% Output
%   [thresh_adj] = NxN adjacency after thresholding
%% threshold coherence
thresh=double(Adj>=Coh)+double(Adj<=-Coh);
% thresh=double(Adj>=Coh);
thresh_adj=Adj.*thresh;

%% threshold distance
for m = 1:length(Coord)
    for n = 1:length(Coord)
        D = sqrt((Coord(m,1)-Coord(n,1))^2+(Coord(m,2)-Coord(n,2))^2);
        if D>d && thresh_adj(m,n)<0.8
            thresh_adj(m,n)=0;

        elseif D>d && thresh_adj(m,n)>=0.8
            thresh_adj(m,n)=thresh_adj(m,n)*(1/sqrt(d));
        end
    end
end
end