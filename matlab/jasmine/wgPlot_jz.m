function [hE]=wgPlot_jz(adjMat,coord,large_coord,change_index,M1,S1,max_w,min_w,laser,date,ind,varargin)
%{
Weighted Graph Plot from adjacency matrix [adjMat] and vertices coordinate [coord].

INPUT:
    [adjMat] = N x N sparse square adjacency matrix.
    [coord] = N x 2 matrix of coordinates of good electrode to be plotted.
    [large_coord] = M x 2 matrix of coordinates of all ECoG electrodes.
    [change_index] = vector of electrode indices with greatest coherence change 
    [M1] = vector of electrode indices in M1 region
    [S1] = vector of electrode indices in S1 region
    [max_w] = scalar of maximum edge weight (max coherence/change allowed)
    [min_w] = scalar of minimum edge weight (min coherence/change allowed)
    [laser] = vector of electrode indices for laser location
    [date] = string indicating experimental session
    [date] = string indicating recording/conditioning block#
    

    [varargin] = are specified as parameter value pairs where the parameters are
    'edgeColorMap' = m1 x 3 edge colormap for coloring edges by their weight, {default=cool}
    'edgeWidth' = scalar specifying the width of the edges. {default=0.1}
    'vertexColorMap' = m2 x 3 vertex color map for coloring vertices by their weight, {default=summer}
    'vertexWeight' = N x 1 vector of vertex weight that overrides using the diagonal of [adjMat] for specifying vertex size.
    'vertexScale' = scalar vertex scaling factor for specifying scaling the size of vertices. {default=100}
    'vertexmetadata' = N x 1 vector of vertex meta data for coloring the verticies.

OUTPUT:
    [hE] = vector/scalar of handles to edges of the drawn graph (1 per color).

Adapted from michael.wu@lithium.com (May 2009)
%}


%% Set default parameter values
hold on; axis off;
h=gca;
axesArea(h,[6 10 6 8]); %set [left, lower, right, upper] margins
plotParm={'markerSize',6,'lineWidth',0.1,'marker','.','MarkerEdgeColor',[1,0.5,0.2]};
edgeMap=cool;


% Parse parameter value pairs
nVarArgin=length(varargin);
for kk=1:2:nVarArgin
    switch lower(varargin{kk})
        case 'edgecolormap'
            edgeMap=varargin{kk+1};
        case 'edgewidth'
            plotParm=[plotParm,{'lineWidth',varargin{kk+1}}];
        case 'vertexcolormap'
            vrtxMap=varargin{kk+1};
        case 'vertexweight'
            vrtxWt=varargin{kk+1};
        case 'vertexmetadata'
            vrtxCol=varargin{kk+1};
        case 'vertexscale'
            vrtxSiz=varargin{kk+1};
        otherwise
            error(['wgPlot_jz Unknown parameter',varargin{kk},'.']) ;
    end
end

%% Find edge index
[ii,jj,eWt] = find(adjMat);% convert adjacency matrix to edge list
if isempty(max_w)
    minEWt=min(eWt);
    maxEWt=max(eWt);
else
    minEWt=min_w;
    maxEWt=max_w;
end
% eWtRange=maxEWt-minEWt;
% eWeighted=eWtRange>0;

% Map edge weight (0-1) to colormap
neColor=size(edgeMap,1);
% normalize edge weight (acros all blocks) and match to colormap
eWt_map=ceil((neColor-1)*(eWt-minEWt)/(maxEWt-minEWt)+1);

% % Fit to a blue/white/red colormap
% eWt_map=zeros(size(eWt));
% for n=1:length(eWt)
%     if eWt(n)>=0
%         eWt_map(n)=neColor/2+ceil(neColor/2*eWt(n)/maxEWt);
%     else
%         eWt_map(n)=neColor/2-ceil(neColor/2*eWt(n)/minEWt)+1;
%     end
%     
%     if eWt_map(n)>neColor
%         eWt_map(n)=neColor;
%     end
% end
%}
%% Plot weighted edges

hE=[];
%find the boundary of nodes with edges
maxX=max(coord(:,1));
minX=min(coord(:,1));
maxY=max(coord(:,2));
minY=min(coord(:,2));

legLim=linspace(minX,maxX,neColor+1);
yStep=(maxY-minY)/12; %empty space between data, legends, and label
legY=minY-yStep; %legend location

for kk=1:neColor
    p=find(eWt_map==kk);
    nSegment=length(p);
    x=[coord(ii(p),1),coord(jj(p),1),nan(nSegment,1)]';
    y=[coord(ii(p),2),coord(jj(p),2),nan(nSegment,1)]';
    % hE=[hE,plot(x(:),y(:),'color',edgeMap(kk,:),plotParm{:})];
    patchline(x(:),y(:),'edgecolor',edgeMap(kk,:),'linewidth',1.5,'edgealpha',0.8*kk/neColor)
    % alim([min(adjMat(:)) max(adjMat(:))])
    % Draw legend bar
    line([legLim(kk),legLim(kk+1)],[legY,legY],'lineWidth',15,'color',edgeMap(kk,:));
end  % for kk

% Draw Legend Label
text([legLim(1),legLim(floor(end/2))+0.3,legLim(end)],...
    [legY-yStep,legY-yStep,legY-yStep], ...
    {num2str(minEWt),'\Delta Theta Coherence',num2str(maxEWt)},...
    'HorizontalAlignment','center','FontSize',18,'color',[0.2,0.2,0.2]);

%% plot electrodes at vertices

% scatter(coord(:,1),coord(:,2),100,'filled','MarkerFaceColor',[0.5,0.5,0.5]); 

% M1 in blue
scatter(large_coord(M1,1),large_coord(M1,2),150,'filled','MarkerFaceColor',[0.3,0.65,0.8])
% S1 in green
scatter(large_coord(S1,1),large_coord(S1,2),150,'filled','MarkerFaceColor',[0.3,0.6,0.4])

%% plot lasers
if length(laser)==2
    % laser in blue
    scatter(large_coord(laser,1),large_coord(laser,2),500,'filled','MarkerFaceColor',[0.4, 0.2, 0.9]);
    % label the lasers
    text(large_coord(laser,1),large_coord(laser,2),{' L1','L2'},'FontWeight','bold',...
        'HorizontalAlignment','center','FontSize',7,'color',[1,1,1]);
    text(large_coord(laser(2),1),large_coord(laser(2),2)+0.7,'+100ms','FontWeight','bold',...
        'HorizontalAlignment','center','FontSize',10,'color',[0.3,0.3,0.3]);
elseif length(laser)==1
    % laser in purple
    scatter(large_coord(laser,1),large_coord(laser,2),500,'filled','MarkerFaceColor',[0.4, 0.2, 0.9]);
    % label the lasers
    text(large_coord(laser,1),large_coord(laser,2),{'L1'},'FontWeight','bold',...
        'HorizontalAlignment','center','FontSize',11,'color',[1,1,1]);
end
%% plot location of greatest change
if ~isempty(change_index)
    f1=scatter(coord(change_index,1),coord(change_index,2),500,'filled','MarkerFaceColor',[0.4,0,0.2]);
    f1.MarkerFaceAlpha = 0.4;
end
%% axis and title
title(join([date,ind]),'FontSize',18)
axis tight;
ax=axis;
dxRange=(ax(2)-ax(1))/500;
dyRange=(ax(4)-ax(3))/500;
axis([ax(1)-dxRange,ax(2)+dxRange,ax(3)-dyRange,ax(4)+dyRange]);
hold off