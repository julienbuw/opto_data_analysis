function find_num_active_electrode
load('sorted_data_J.mat','single_laser','long_delay','short_delay','control');
% This function finds the # of electrode with absolute changes in 
% network coherence above threshold, for each conditioning frame (20s)
% then plots the results for single laser, long delay, short delay
% and control sessions

% set 2 thresholding methods for delta coherence
c1=0.1;c2=1;

% get #sessions for each stimulation pattern
num_single = length(single_laser(:,1));
num_long = length(long_delay(:,1));
num_short = length(short_delay(:,1));
num_control = length(control(:,1));

% initialize matrices for each stimulation pattern
num_active=zeros(1,5);
total_active_single=[];total_ratio_single=[];
total_active_long=[];total_ratio_long=[];
total_active_short=[];total_ratio_short=[];
total_active_control=[];total_ratio_control=[];

%% single laser sessions
for n = 1:num_single
    %set good channels
    good_chl=single_laser{n,3};
    abs_delta=zeros(5,length(good_chl));
    %set recording blocks
    rec_data=single_laser{n,1};
    %set conditioning blocks
    cond_data=single_laser{n,2};
    
    % calculate average coh for baseline
    if ndims(rec_data)==3
        adj_base = rec_data(:,:,1);
    else
        adj_base = squeeze(mean(rec_data(:,:,:,1)));
    end
    adj_base=adj_base(good_chl,good_chl);
    adj_base=adj_base-eye(size(adj_base));
    
    % CondBlock1
    adj_cond=squeeze(mean(cond_data(:,:,:,1)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    abs_delta(1,:)=sum(abs(adj_delta));%absolute delta coh for each electrode
%     adj_pos1=adj_delta;
%     adj_pos1(adj_pos1<0)=0;
%     pos_cond1=sum(adj_pos1);
    
    % CondBlock2
    adj_cond=squeeze(mean(cond_data(:,:,:,2)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    abs_delta(2,:)=sum(abs(adj_delta));
%     adj_pos2=adj_delta;
%     adj_pos2(adj_pos2<0)=0;
%     pos_cond2=sum(adj_pos2);

    % CondBlock3
    adj_cond=squeeze(mean(cond_data(:,:,:,3)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    abs_delta(3,:)=sum(abs(adj_delta));
%     adj_pos3=adj_delta;
%     adj_pos3(adj_pos3<0)=0;
%     pos_cond3=sum(adj_pos3);
    
    % CondBlock4
    adj_cond=squeeze(mean(cond_data(:,:,:,4)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    abs_delta(4,:)=sum(abs(adj_delta));    
%     adj_pos4=adj_delta;
%     adj_pos4(adj_pos4<0)=0;
%     pos_cond4=sum(adj_pos4);
    
    % CondBlock5
    adj_cond=squeeze(mean(cond_data(:,:,:,5)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    abs_delta(5,:)=sum(abs(adj_delta));    
%     adj_pos5=adj_delta;
%     adj_pos5(adj_pos5<0)=0;
%     pos_cond5=sum(adj_pos5);
    
    final_change= abs_delta;
    
    % final_change=[pos_cond1;pos_cond2;pos_cond3;pos_cond4;pos_cond5];
    
    % use threshold method 1 for delta coherence
    alpha_coh_1=sum(sum(adj_base))/length(good_chl)*c1;
    
    % threshold method 2
    % alpha_coh_2=sum(abs_delta(1,:))/length(good_chl)*c2;
    
    % find #electrode with delta coh above threshold for each condblock
    for ii=1:5
        change_index=find(final_change(ii,:)>alpha_coh_1);
        num_active(ii)=length(change_index);
    end
    % collect results for all single laser sessions
    total_active_single=[total_active_single;num_active];
    ratio_single=num_active(2:end)./num_active(1);
    total_ratio_single=[total_ratio_single;ratio_single];
end 
average_active_single=mean(total_active_single)'; %calculate the mean #
sem_single=std(total_active_single).'/sqrt(num_single); %calculate standard error
average_ratio_single=mean(total_ratio_single)'; %calculate the mean of normalized #
sem_ratio_single=std(total_ratio_single).'/sqrt(num_single); %calculate standard error

%% long latency sessions
for n = 1:num_long
    %set good channels
    good_chl=long_delay{n,3};
    abs_delta=zeros(5,length(good_chl));
    %set recording blocks
    rec_data=long_delay{n,1};
    %set conditioning blocks
    cond_data=long_delay{n,2};
    
    % calculate average coh for baseline
    if ndims(rec_data)==3
        adj_base = rec_data(:,:,1);
    else
        adj_base = squeeze(mean(rec_data(:,:,:,1)));
    end
    adj_base=adj_base(good_chl,good_chl);
    adj_base=adj_base-eye(size(adj_base));
    
    % CondBlock1
    adj_cond=squeeze(mean(cond_data(:,:,:,1)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos1=adj_delta;
    adj_pos1(adj_pos1<0)=0;
    pos_cond1=sum(adj_pos1);
    abs_delta(1,:)=sum(abs(adj_delta));
    
    % CondBlock2
    adj_cond=squeeze(mean(cond_data(:,:,:,2)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos2=adj_delta;
    adj_pos2(adj_pos2<0)=0;
    pos_cond2=sum(adj_pos2);
    abs_delta(2,:)=sum(abs(adj_delta));
    
    % CondBlock3
    adj_cond=squeeze(mean(cond_data(:,:,:,3)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos3=adj_delta;
    adj_pos3(adj_pos3<0)=0;
    pos_cond3=sum(adj_pos3);
    abs_delta(3,:)=sum(abs(adj_delta));
    
    % CondBlock4
    adj_cond=squeeze(mean(cond_data(:,:,:,4)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos4=adj_delta;
    adj_pos4(adj_pos4<0)=0;
    pos_cond4=sum(adj_pos4);
    abs_delta(4,:)=sum(abs(adj_delta));
    
    % CondBlock5
    adj_cond=squeeze(mean(cond_data(:,:,:,5)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos5=adj_delta;
    adj_pos5(adj_pos5<0)=0;
    pos_cond5=sum(adj_pos5);
    abs_delta(5,:)=sum(abs(adj_delta));
    
    final_change= abs_delta;
    % final_change=[pos_cond1;pos_cond2;pos_cond3;pos_cond4;pos_cond5];
    
    % set threshold methods
    alpha_coh_1=sum(sum(adj_base))/length(good_chl)*c1;
    alpha_coh_2=sum(abs_delta(1,:))/length(good_chl)*c2;
    
    for ii=1:5
        change_index=find(final_change(ii,:)>alpha_coh_1);
        num_active(ii)=length(change_index);
    end

    total_active_long=[total_active_long;num_active];
    ratio_long=num_active(2:end)./num_active(1);
    total_ratio_long=[total_ratio_long;ratio_long];
end
average_active_long=mean(total_active_long)';
sem_long=std(total_active_long).'/sqrt(num_long);
average_ratio_long=mean(total_ratio_long)';
sem_ratio_long=std(total_ratio_long).'/sqrt(num_long);

%% short latency sessions
for n = 1:num_short
    
    %set good channels
    good_chl=short_delay{n,3};
    abs_delta=zeros(5,length(good_chl));
    %set recording blocks
    rec_data=short_delay{n,1};
    %set conditioning blocks
    cond_data=short_delay{n,2};
    % calculate average coh for baseline
    if ndims(rec_data)==3
        adj_base = rec_data(:,:,1);
    else
        adj_base = squeeze(mean(rec_data(:,:,:,1)));
    end
    adj_base=adj_base(good_chl,good_chl);
    adj_base=adj_base-eye(size(adj_base));
    
    % CondBlock1
    adj_cond=squeeze(mean(cond_data(:,:,:,1)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos1=adj_delta;
    adj_pos1(adj_pos1<0)=0;
    pos_cond1=sum(adj_pos1);
    abs_delta(1,:)=sum(abs(adj_delta));
    
    % CondBlock2
    adj_cond=squeeze(mean(cond_data(:,:,:,2)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos2=adj_delta;
    adj_pos2(adj_pos2<0)=0;
    pos_cond2=sum(adj_pos2);
    abs_delta(2,:)=sum(abs(adj_delta));
    
    % CondBlock3
    adj_cond=squeeze(mean(cond_data(:,:,:,3)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos3=adj_delta;
    adj_pos3(adj_pos3<0)=0;
    pos_cond3=sum(adj_pos3);
    abs_delta(3,:)=sum(abs(adj_delta));
    
    % CondBlock4
    adj_cond=squeeze(mean(cond_data(:,:,:,4)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos4=adj_delta;
    adj_pos4(adj_pos4<0)=0;
    pos_cond4=sum(adj_pos4);
    abs_delta(4,:)=sum(abs(adj_delta));
    
    % CondBlock5
    adj_cond=squeeze(mean(cond_data(:,:,:,5)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos5=adj_delta;
    adj_pos5(adj_pos5<0)=0;
    pos_cond5=sum(adj_pos5);
    abs_delta(5,:)=sum(abs(adj_delta));
    
    final_change= abs_delta;
    % final_change=[pos_cond1;pos_cond2;pos_cond3;pos_cond4;pos_cond5];

    alpha_coh_1=sum(sum(adj_base))/length(good_chl)*c1;
    alpha_coh_2=sum(abs_delta(1,:))/length(good_chl)*c2;
    
    for ii=1:5
        change_index=find(final_change(ii,:)>alpha_coh_1);
        num_active(ii)=length(change_index);
    end

    total_active_short=[total_active_short;num_active];
    ratio_short=num_active(2:end)./num_active(1);
    total_ratio_short=[total_ratio_short;ratio_short];
end
average_active_short=mean(total_active_short)';
sem_short=std(total_active_short).'/sqrt(num_short);
average_ratio_short=mean(total_ratio_short)';
sem_ratio_short=std(total_ratio_short).'/sqrt(num_short);

%% control sessions
for n = 1:num_control
    %set good channels
    good_chl=control{n,3};
    abs_delta=zeros(5,length(good_chl));
    %set recording blocks
    rec_data=control{n,1};
    %set conditioning blocks
    cond_data=control{n,2};
    % calculate average coh for baseline
    if ndims(rec_data)==3
        adj_base = rec_data(:,:,1);
    else
        adj_base = squeeze(mean(rec_data(:,:,:,1)));
    end
    adj_base=adj_base(good_chl,good_chl);
    adj_base=adj_base-eye(size(adj_base));
    
    % CondBlock1
    adj_cond=squeeze(mean(cond_data(:,:,:,1)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos1=adj_delta;
    adj_pos1(adj_pos1<0)=0;
    pos_cond1=sum(adj_pos1);
    abs_delta(1,:)=sum(abs(adj_delta));
    
    % CondBlock2
    adj_cond=squeeze(mean(cond_data(:,:,:,2)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos2=adj_delta;
    adj_pos2(adj_pos2<0)=0;
    pos_cond2=sum(adj_pos2);
    abs_delta(2,:)=sum(abs(adj_delta));
    
    % CondBlock3
    adj_cond=squeeze(mean(cond_data(:,:,:,3)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos3=adj_delta;
    adj_pos3(adj_pos3<0)=0;
    pos_cond3=sum(adj_pos3);
    abs_delta(3,:)=sum(abs(adj_delta));
    
    % CondBlock4
    adj_cond=squeeze(mean(cond_data(:,:,:,4)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos4=adj_delta;
    adj_pos4(adj_pos4<0)=0;
    pos_cond4=sum(adj_pos4);
    abs_delta(4,:)=sum(abs(adj_delta));
    
    % CondBlock5
    adj_cond=squeeze(mean(cond_data(:,:,:,5)));
    adj_cond=adj_cond(good_chl,good_chl);
    adj_cond=adj_cond-eye(size(adj_cond));
    adj_delta=adj_cond-adj_base;%get delta coherence
    adj_pos5=adj_delta;
    adj_pos5(adj_pos5<0)=0;
    pos_cond5=sum(adj_pos5);
    abs_delta(5,:)=sum(abs(adj_delta));
    
    final_change= abs_delta;
    % final_change=[pos_cond1;pos_cond2;pos_cond3;pos_cond4;pos_cond5];

    alpha_coh_1=sum(sum(adj_base))/length(good_chl)*c1;
    alpha_coh_2=sum(abs_delta(1,:))/length(good_chl)*c2;
   
    for ii=1:5
        change_index=find(final_change(ii,:)>alpha_coh_1);
        num_active(ii)=length(change_index);
    end

    total_active_control=[total_active_control;num_active];
    ratio_control=num_active(2:end)./num_active(1);
    total_ratio_control=[total_ratio_control;ratio_control];
end
average_active_control=mean(total_active_control)';
sem_control=std(total_active_control).'/sqrt(num_control);
average_ratio_control=mean(total_ratio_control)';
sem_ratio_control=std(total_ratio_control).'/sqrt(num_control);

%% plot results for each conditioning blocks
figure;
X=[1 2 3 4 5];
Y=[average_active_control average_active_single average_active_long average_active_short];
err=[sem_control sem_single sem_long sem_short];
bar(X,Y,'FaceAlpha',0.7)
set(gca, 'XTickLabel', {'CondBlock1','CondBlock2','CondBlock3','CondBlock4','CondBlock5'});
hold on
ngroups = size(Y, 1);
nbars = size(Y, 2);
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    ebar=errorbar(x, Y(:,i), err(:,i), '.');
    ebar.Color = [0 0 0];
    ebar.LineWidth = 1.5;
end
legend('control','single laser','long delay','short delay','Location','northwest')
fig = gcf; % current figure handle
fig.Color = [1 1 1];
hold off
ylabel('#Electrodes with absolute \Delta Coh above threshold')

%% plot ratio to conditioning block 1 (normalized results)
figure;
X=[1 2 3 4];
Y=[average_ratio_control average_ratio_single average_ratio_long average_ratio_short];
err=[sem_ratio_control sem_ratio_single sem_ratio_long sem_ratio_short];
bar(X,Y,'FaceAlpha',0.8)
set(gca, 'XTickLabel', {'#block2/1','#block3/1','#block4/1','#block5/1'});
hold on
ngroups = size(Y, 1);
nbars = size(Y, 2);
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    ebar=errorbar(x, Y(:,i), err(:,i), '.');
    ebar.Color = [0 0 0];
    ebar.LineWidth = 1.5;
end
legend('control','single laser','long delay','short delay','Location','northwest')
fig = gcf; % current figure handle
fig.Color = [1 1 1];
hold off
ylabel('Normalized increase in #electrodes above threshold')
end
