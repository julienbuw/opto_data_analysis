function [center_coord] = find_center(coords, delta_coh)
% find the center of mass of network change
% input
%   [coords] = coordinates of good electrodes
%   [dealta_coh] = change in coherence adjacency matrix
% output
%   [center_coord] = 1x2 vector of coordinates of centroid

center_x=sum(coords(:,1).*delta_coh)/sum(delta_coh);
center_y=sum(coords(:,2).*delta_coh)/sum(delta_coh);
center_coord=[center_x center_y];