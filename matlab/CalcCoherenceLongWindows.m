function CalcCoherenceLongWindows(name,sessions)
%% Calculate Coherence
% different monkeys have data in different places
if strcmp(name,'Jalapeno'), drive = 2;
else drive = 3;
end
if ismac, basedir = '/Volumes';
else basedir = '\\minnie.cin.ucsf.edu';
end
basedir = fullfile(basedir,sprintf('data%i',drive),...
    'PairedStimJalapeno','TDT_Blocks',name);

for s=1:length(sessions)
    date = sessions(s).date;
    session = sessions(s).session;
    array = sessions(s).arrays{1};
    
    for rec_block=1:6, % Each recording block
        
        % data directory
        datadir = fullfile(basedir,date,'Processed',...
            sprintf('Session%i',session),array,'RecordingBlocks');
        datafile = sprintf('RecBlock%i.mat',rec_block);
        f = load(fullfile(datadir,datafile),'samp_freq');
        
        % saving directory
        savedir = fullfile(basedir,date,'Processed',...
            sprintf('Session%i',session),array,'CoherenceLongWindows');
        savefile = sprintf('block%i.mat',rec_block);
        if ~exist(savedir,'dir'), mkdir(savedir); end
        sf = matfile(fullfile(savedir,savefile),'Writable',true);
        
        % filter parameters
        samp_freq = f.samp_freq;
        
        % calculate cross spectrum
        Nwin = round(10*samp_freq);
        ovlp = round(Nwin/2);
        Nfft = 3000;
        
        coh(1:96,1:96) = struct(...
            'mag',[],...
            'phase',[]);
        
        fprintf('\n%s\n',date)
        fprintf('  - Session %i', session)
        
        fprintf('\n    * Rec Block %i of 6: ',rec_block)
        msg = sprintf('ch%02i <--> ch%02i',1,2);
        fprintf('%s',msg)
        
        for ch1 = 1:96,
            f = load(fullfile(datadir,datafile),sprintf('lfp_ch%02i',ch1));
            sig1 = f.(sprintf('lfp_ch%02i',ch1)); % load channel
            for ch2 = ch1+1:96,
                f = load(fullfile(datadir,datafile),sprintf('lfp_ch%02i',ch2));
                sig2 = f.(sprintf('lfp_ch%02i',ch2)); % load channel
                
                % built in matlab coherence and cross spectrum code
                [Cxy,~] = cpsd(sig1,sig2,hamming(Nwin),ovlp,Nfft,samp_freq);
                [mag,F] = mscohere(sig1,sig2,hamming(Nwin),ovlp,Nfft,samp_freq);
                
                % store variables
                coh(ch1,ch2).mag = mag;
                coh(ch1,ch2).phase = angle(Cxy);
                
                % output progress
                fprintf(repmat('\b',1,length(msg)))
                msg = sprintf('ch%02i <--> ch%02i',ch1,ch2);
                fprintf('%s',msg)
                
            end
        end
        fprintf('\n')
        
        sf.coh = coh;
        sf.freq = F;
        
    end % recording block
end % session



