% for n1=1:29 
%     for n2=1:2
%         if ~isempty(j(n1).stim_Coh{n2})
%             disp(~isempty(j(n1).stim_Coh(n2)))
%             disp(n1)
%             disp(n2)
%             las(n1,n2)=j(n1).stim_Coh{n2};
%         end
%     end
% end

j = get_sessions('Jalapeno', []);

j_m1=zeros(length(j), 96);
for n1=1:length(j)
    for n2=1:length(j(n1).M1_sites)
        j_m1(n1, n2) = j(n1).M1_sites(n2);
    end
end


j_s1=zeros(length(j), 96);
for n1=1:length(j)
    for n2=1:length(j(n1).S1_sites)
        j_s1(n1, n2) = j(n1).S1_sites(n2);
    end
end

% Get stim_Coh
j_stim_Coh=zeros(length(j), 2);
for n1=1:length(j)
    for n2=1:length(j(n1).stim_Coh)
        temp = j(n1).stim_Coh{n2};
        if ~isempty(temp)
            j_stim_Coh(n1, n2) = temp;
        end
    end
end

g = get_sessions('GT', []);

g_m1=zeros(length(g), 96);
for n1=1:length(g)
    for n2=1:length(g(n1).M1_sites)
        g_m1(n1, n2) = g(n1).M1_sites(n2);
    end
end

g_s1=zeros(length(g), 96);
for n1=1:length(g)
    for n2=1:length(g(n1).S1_sites)
        g_s1(n1, n2) = g(n1).S1_sites(n2);
    end
end

% Get stim_Coh
g_stim_Coh=zeros(length(g), 2);
for n1=1:length(g)
    for n2=1:length(g(n1).stim_Coh)
        temp = g(n1).stim_Coh{n2};
        if ~isempty(temp)
            g_stim_Coh(n1, n2) = temp;
        end
    end
end