function sessions = get_sessions(name, delay)
%sessions = get_sessions(name, delay)
%   Outputs array of structs, where each struct contains information for
%   one experimental session of the paired stim study. Use inputs to select
%   sessions for each monkey or with a given delay btw stim sites
% 
%   name is either 'GT' or 'Jalapeno'  
%   delay is given in ms,
%       valid delays are 0,10,30,70,100, and [], 0 corresponds to
%       single-site stim sessions and an empty vector grabs sessions with
%       all delays

% if delay is empty vector, then output all sessions
if isempty(delay)
    delay = 0;
    all = 1;
else
    all = 0;
end

% GT
days(1).name        = 'GT';
days(1).date        = '20150908';
days(1).sessions    = [2,3,4];
days(1).arrays      = {{'M1'},{'M1'},{'M1'}};
days(1).delays      = [10,10,10];
days(1).orientation = {'no photo','perp','par'};
days(1).area        = {{'M1','M1'},{'M1','M1'},{'M1','M1'}};
days(1).laser       = {1:2,1:2,1:2};
days(1).stim_ER     = {{47,63},{45,43},{25,[]}};
days(1).stim_Coh    = {{46,63},{48,43},{48,16}};
days(1).M1_sites    = [1:48,49:2:55];

days(2).name        = 'GT';
days(2).date        = '20150909';
days(2).sessions    = [2,3,4,5];
days(2).arrays      = {{'M1'},{'M1'},{'M1'},{'M1'}};
days(2).delays      = [10,10,10,70];
days(2).orientation = {'par','par','par','par'};
days(2).area        = {{'M1','M1'},{'M1','M1'},{'M1','M1'},{'M1','M1'}};
days(2).laser       = {1:2,1:2,1:2,1:2};
days(2).stim_ER     = {{48,12},{53,51},{43,40},{14,55}};
days(2).stim_Coh    = {{48,12},{53,27},{46,16},{14,55}};
days(2).M1_sites    = [1:2:31,2:2:8,12:2:32,38:2:48,39:2:55,69,71,50,52];

days(3).name        = 'GT';
days(3).date        = '20150910';
days(3).sessions    = [3,4,6,7];
days(3).arrays      = {{'S1'},{'S1'},{'S1'},{'S1'}};
days(3).delays      = [70,70,10,10];
days(3).orientation = {'par','par','par','perp'};
days(3).area        = {{'M1','M1'},{'M1','M1'},{'M1','M1'},{'M1','M1'}};
days(3).laser       = {1:2,1:2,1:2,1:2};
days(3).stim_ER     = {{36,41},{8,4},{62,71},{64,25}};
days(3).stim_Coh    = {{36,42},{8,4},{62,71},{64,46}};
days(3).M1_sites    = [1:48,49:2:55,65:2:71];

days(4).name        = 'GT';
days(4).date        = '20150911';
days(4).sessions    = [2,3,4,5,6,7];
days(4).arrays      = {{'S1'},{'S1'},{'S1'},{'S1'},{'S1'},{'S1'}};
days(4).delays      = [-1,0,0,-1,30,30];
days(4).orientation = {'none','single','single','none','perp','par'};
days(4).area        = {{'M1','M1'},{'M1',''},{'','M1'},{'M1','M1'},{'M1','M1'},{'M1','M1'}};
days(4).laser       = {1:2,1,2,1:2,1:2,1:2};
days(4).stim_ER     = {{48,37},{48,37},{48,37},{65,29},{65,29},{59,67}};
days(4).stim_Coh    = {{48,37},{48,37},{48,37},{65,29},{65,47},{33,53}};
days(4).M1_sites    = [1:48,49:2:55];

days(5).name        = 'GT';
days(5).date        = '20150914';
days(5).sessions    = [1,2,3];
days(5).arrays      = {{'S1'},{'S1'},{'S1'}};
days(5).delays      = [10,100,100];
days(5).orientation = {'par','par','par'};
days(5).area        = {{'M1','M1'},{'M1','M1'},{'M1','M1'}};
days(5).laser       = {1:2,1:2,1:2};
days(5).stim_ER     = {{57,64},{[],[]},{35,[]}};
days(5).stim_Coh    = {{33,64},{[],[]},{35,28}};
days(5).M1_sites    = [1:32,34:2:48,37:2:55];

days(6).name        = 'GT';
days(6).date        = '20150915';
days(6).sessions    = [2,3,4,5];
days(6).arrays      = {{'S1'},{'S1'},{'S1'},{'S1'}};
days(6).delays      = [30,10,30,10];
days(6).orientation = {'both','par','par','par'};
days(6).area        = {{'M1','M1'},{'M1','M1'},{'M1','M1'},{'M1','M1'}};
days(6).laser       = {1:2,1:2,1:2,1:2};
days(6).stim_ER     = {{36,20},{28,37},{30,29},{33,42}};
days(6).stim_Coh    = {{36,20},{28,37},{30,59},{33,42}};
days(6).M1_sites    = [1:48,49:2:71,50:2:56];

days(7).name        = 'GT';
days(7).date        = '20150916';
days(7).sessions    = [4];
days(7).arrays      = {{'S1'}};
days(7).delays      = [10];
days(7).orientation = {'par (close)'};
days(7).area        = {{'S1','S1'}};
days(7).laser       = {1:2};
days(7).stim_ER     = {{35,36}};
days(7).stim_Coh    = {{35,36}};
days(7).M1_sites    = [];

days(8).name        = 'GT';
days(8).date        = '20150917';
days(8).sessions    = [1,2,3];
days(8).arrays      = {{'S1','M1'},{'S1','M1'},{'S1','M1'}};
days(8).delays      = [10,10,100];
days(8).orientation = {'par','par (close)','both'};
days(8).area        = {{'S1','S1'},{'S1','S1'},{'S1','S1'}};
days(8).laser       = {1:2,1:2,1:2};
days(8).stim_ER     = {{53,18},{37,61},{24,46}};
days(8).stim_Coh    = {{53,18},{37,61},{24,46}};
days(8).M1_sites    = 1:96;

days(9).name        = 'GT';
days(9).date        = '20150918';
days(9).sessions    = [1];
days(9).arrays      = {{'S1','M1'}};
days(9).delays      = [100];
days(9).orientation = {'perp'};
days(9).area        = {{'S1','S1'}};
days(9).laser       = {1:2};
days(9).stim_ER     = {{71,64}};
days(9).stim_Coh    = {{71,64}};
days(9).M1_sites    = 1:96;

days(10).name        = 'GT';
days(10).date        = '20150921';
days(10).sessions    = [3,5];
days(10).arrays      = {{'S1'},{'S1'}};
days(10).delays      = [10,10];
days(10).orientation = {'across','across'};
days(10).area        = {{'M1','S1'},{'S1','M1'}};
days(10).laser       = {1:2,1:2};
days(10).stim_ER     = {{28,76},{51,39}};
days(10).stim_Coh    = {{28,76},{51,39}};
days(10).M1_sites    = [1:32,34:2:48,33:2:47,57,59];

days(11).name        = 'GT';
days(11).date        = '20150922';
days(11).sessions    = [1,2,3];
days(11).arrays      = {{'S1'},{'S1'},{'S1'}};
days(11).delays      = [10,10,100];
days(11).orientation = {'across','across','across'};
days(11).area        = {{'S1','M1'},{'S1','M1'},{'M1','S1'}};
days(11).laser       = {1:2,1:2,1:2};
days(11).stim_ER     = {{80,39},{[],[]},{43,62}};
days(11).stim_Coh    = {{80,39},{59,36},{43,62}};
days(11).M1_sites    = [1:32,34:2:48,33:2:39,57];

days(12).name        = 'GT';
days(12).date        = '20150925';
days(12).sessions    = [1,2];
days(12).arrays      = {{'S1'},{'S1'}};
days(12).delays      = [10,10];
days(12).orientation = {'par','both (close)'};
days(12).area        = {{'S1','S1'},{'S1','S1'}};
days(12).laser       = {1:2,1:2};
days(12).stim_ER     = {{68,57},{39,80}};
days(12).stim_Coh    = {{56,57},{39,80}};
days(12).M1_sites    = [1:32,34:2:48];

% Jalapeno
days(13).name        = 'Jalapeno';
days(13).date        = '20160425';
days(13).sessions    = [1,2,3];
days(13).arrays      = {{'M1'},{'M1'},{'S1'}};
days(13).delays      = [0,0,0];
days(13).orientation = {'single','single','single'};
days(13).area        = {{'S1',''},{'M1',''},{'S1',''}};
days(13).laser       = {1,1,1};
days(13).stim_ER     = {{65,[]},{38,[]},{[],[]}};
days(13).stim_Coh    = {{56,[]},{37,[]},{[],[]}};
days(13).M1_sites    = [1:32,34:2:48];

days(14).name        = 'Jalapeno';
days(14).date        = '20160426';
days(14).sessions    = [1,2,3];
days(14).arrays      = {{'S1'},{'S1'},{'S1'}};
days(14).delays      = [30,10,10];
days(14).orientation = {'par','par','par'};
days(14).area        = {{'M1','M1'},{'M1','M1'},{'M1','M1'}};
days(14).laser       = {1:2,1:2,1:2};
days(14).stim_ER     = {{12,41},{43,12},{36,47}};
days(14).stim_Coh    = {{12,43},{55,12},{36,47}};
days(14).M1_sites    = [1:24,26:2:32,42:2:48];

days(15).name        = 'Jalapeno';
days(15).date        = '20160427';
days(15).sessions    = [2];
days(15).arrays      = {{'S1'}};
days(15).delays      = [0];
days(15).orientation = {'single'};
days(15).area        = {{'S1',''}};
days(15).laser       = {1};
days(15).stim_ER     = {{39,[]}};
days(15).stim_Coh    = {{63,[]}};
days(15).M1_sites    = [1:24,26:2:48];

days(16).name        = 'Jalapeno';
days(16).date        = '20160428';
days(16).sessions    = [2,3];
days(16).arrays      = {{'S1'},{'S1'}};
days(16).delays      = [100,10];
days(16).orientation = {'perp','par'};
days(16).area        = {{'M1','M1'},{'M1','M1'}};
days(16).laser       = {1:2,1:2};
days(16).stim_ER     = {{15,[]},{37,44}};
days(16).stim_Coh    = {{15,40},{38,27}};
days(16).M1_sites    = [1:32,34:2:48];

days(17).name        = 'Jalapeno';
days(17).date        = '20160429';
days(17).sessions    = [1,3];
days(17).arrays      = {{'S1'},{'S1'}};
days(17).delays      = [10,100];
days(17).orientation = {'par','perp'};
days(17).area        = {{'M1','M1'},{'M1','M1'}};
days(17).laser       = {1:2,1:2};
days(17).stim_ER     = {{44,37},{39,[]}};
days(17).stim_Coh    = {{42,37},{39,16}};
days(17).M1_sites    = [1:32,34:2:48];

days(18).name        = 'Jalapeno';
days(18).date        = '20160502';
days(18).sessions    = [1];
days(18).arrays      = {{'S1'}};
days(18).delays      = [10];
days(18).orientation = {'perp (across?)'};
days(18).area        = {{'M1','M1'}};
days(18).laser       = {1:2};
days(18).stim_ER     = {{[],16}};
days(18).stim_Coh    = {{35,16}};
days(18).M1_sites    = [1:24,26:2:48];

days(19).name        = 'Jalapeno';
days(19).date        = '20160624';
days(19).sessions    = [1,2,3,4];
days(19).arrays      = {{'S1'},{'S1'},{'S1'},{'S1'}};
days(19).delays      = [0,-1,10,100];
days(19).orientation = {'single','across','across','across'};
days(19).area        = {{'','S1'},{'S1','M1'},{'M1','S1'},{'S1','M1'}};
days(19).laser       = {2,1:2,1:2,1:2};
days(19).stim_ER     = {{[],59},{42,37},{44,37},{35,40}};
days(19).stim_Coh    = {{[],59},{44,61},{44,37},{37,40}};
days(19).M1_sites    = [1:24,26:2:48];

days(20).name        = 'Jalapeno';
days(20).date        = '20160625';
days(20).sessions    = [2,3,4,5];
days(20).arrays      = {{'S1'},{'S1'},{'S1'},{'S1'}};
days(20).delays      = [-1,0,10,10];
days(20).orientation = {'perp','single','par','across'};
days(20).area        = {{'S1','S1'},{'S1',''},{'S1','S1'},{'M1','S1'}};
days(20).laser       = {1:2,1,1:2,1:2};
days(20).stim_ER     = {{65,55},{56,[]},{75,85},{38,90}};
days(20).stim_Coh    = {{65,55},{56,[]},{75,85},{38,90}};
days(20).M1_sites    = [1:24,26:2:48];

days(21).name        = 'Jalapeno';
days(21).date        = '20160626';
days(21).sessions    = [1,2,3];
days(21).arrays      = {{'S1'},{'S1'},{'S1'}};
days(21).delays      = [0,0,0];
days(21).orientation = {'single','single','single'};
days(21).area        = {{'','M1'},{'','M1'},{'','M1'}};
days(21).laser       = {2,2,2};
days(21).stim_ER     = {{[],38},{[],36},{[],28}};
days(21).stim_Coh    = {{[],38},{[],36},{[],28}};
days(21).M1_sites    = [1:24,26:2:48];

days(22).name        = 'Jalapeno';
days(22).date        = '20160627';
days(22).sessions    = [1,2];
days(22).arrays      = {{'S1'},{'S1'}};
days(22).delays      = [10,100];
days(22).orientation = {'across','across'};
days(22).area        = {{'S1','M1'},{'S1','M1'}};
days(22).laser       = {1:2,1:2};
days(22).stim_ER     = {{35,14},{39,14}};
days(22).stim_Coh    = {{35,14},{39,14}};
days(22).M1_sites    = [1:24,26:2:48];

days(23).name        = 'Jalapeno';
days(23).date        = '20160630';
days(23).sessions    = [1,3];
days(23).arrays      = {{'S1'},{'S1'}};
days(23).delays      = [100,10];
days(23).orientation = {'across','across'};
days(23).area        = {{'S1','M1'},{'S1','M1'}};
days(23).laser       = {1:2,1:2};
days(23).stim_ER     = {{62,[]},{58,63}};
days(23).stim_Coh    = {{62,55},{58,63}};
days(23).M1_sites    = [1:24,26:2:38,33];

days(24).name        = 'Jalapeno';
days(24).date        = '20160702';
days(24).sessions    = [2,4];
days(24).arrays      = {{'S1'},{'S1'}};
days(24).delays      = [10,100];
days(24).orientation = {'across','across'};
days(24).area        = {{'S1','M1'},{'S1','M1'}};
days(24).laser       = {1:2,1:2};
days(24).stim_ER     = {{50,37},{44,64}};
days(24).stim_Coh    = {{50,37},{44,64}};
days(24).M1_sites    = [1:24,26:2:32];


% only give sessions for correct monkey
if strcmp(name,'GT'), days = days(1:12);
elseif strcmp(name,'Jalapeno'), days = days(13:end);
else error('Invalid Name');
end

% only give sessions corresponding to delay, 
sessions = [];
for d=1:length(days),
    for b=1:length(days(d).sessions),
        if days(d).delays(b)==delay || all,
            sessions(end+1).name = days(d).name;
            sessions(end).date = days(d).date;
            sessions(end).M1_sites = days(d).M1_sites;
            sessions(end).S1_sites = setdiff(1:96,days(d).M1_sites);
            sessions(end).session = days(d).sessions(b);
            sessions(end).arrays = days(d).arrays{b};
            sessions(end).delay = days(d).delays(b);
            sessions(end).orientation = days(d).orientation{b};
            sessions(end).area = days(d).area{b};
            sessions(end).lasers = days(d).laser{b};
            sessions(end).stim_ER = days(d).stim_ER{b};
            sessions(end).stim_Coh = days(d).stim_Coh{b};
        end
    end
end


