function CalcCoherenceConditioning(name,sessions)
%% Calculate Coherence during conditioning block

% different monkeys have data in different places
if strcmp(name,'Jalapeno'), drive = 2;
else drive = 3;
end
if ismac, basedir = '/Volumes';
else basedir = '\\minnie.cin.ucsf.edu';
end
basedir = fullfile(basedir,sprintf('data%i',drive),...
    'PairedStimJalapeno','TDT_Blocks',name);

for s=1:length(sessions)
    date = sessions(s).date;
    session = sessions(s).session;
    arrays = sessions(s).arrays;
    
    for a=1:length(arrays),
        array = arrays{a};
        for cond_block=1:5, % Each recording block
            
            % data directory
            datadir = fullfile(basedir,date,'Processed',...
                sprintf('Session%i',session),array,'ConditioningBlocks');
            datafile = sprintf('CondBlock%i.mat',cond_block);
            f = load(fullfile(datadir,datafile),'samp_freq');
            
            % saving directory
            savedir = fullfile(basedir,date,'Processed',...
                sprintf('Session%i',session),array,'CoherenceConditioning');
            savefile = sprintf('block%i.mat',cond_block);
            if ~exist(savedir,'dir'), mkdir(savedir); end
            sf = matfile(fullfile(savedir,savefile),'Writable',true);
            
            % filter parameters
            samp_freq = f.samp_freq;
            
            % calculate cross spectrum
            Nwin = round(10*samp_freq);
            ovlp = round(Nwin/2);
            Nfft = 3000;
            
            Cxy(1:96,1:96) = struct(...
                'mag',[],...
                'phase',[]);
            
            fprintf('\n%s\n',date)
            fprintf('  - Session %i', session)
            
            fprintf('\n    * Cond Block %i of 6: ',cond_block)
            msg = sprintf('ch%02i <--> ch%02i',1,2);
            fprintf('%s',msg)
            
            for ch1 = 1:96,
                f = load(fullfile(datadir,datafile),sprintf('lfp_ch%02i',ch1));
                sig1 = f.(sprintf('lfp_ch%02i',ch1)); % load channel
                for ch2 = ch1+1:96,
                    f = load(fullfile(datadir,datafile),sprintf('lfp_ch%02i',ch2));
                    sig2 = f.(sprintf('lfp_ch%02i',ch2)); % load channel
                    
                    % built in matlab coherence and cross spectrum code
                    [coherency,~] = cpsd(sig1,sig2,hamming(Nwin),ovlp,Nfft,samp_freq);
                    [coherence,F] = mscohere(sig1,sig2,hamming(Nwin),ovlp,Nfft,samp_freq);
                    
                    % store variables
                    Cxy(ch1,ch2).mag = coherence;
                    Cxy(ch1,ch2).phase = angle(coherency);
                    
                    % output progress
                    fprintf(repmat('\b',1,length(msg)))
                    msg = sprintf('ch%02i <--> ch%02i',ch1,ch2);
                    fprintf('%s',msg)
                    
                end
            end
            fprintf('\n')
            
            sf.Cxy = Cxy;
            sf.freq = F;
            
        end % recording block
    end % arrays
end % session


