function CalcIntervaledCoherence(name,sessions)
%% Calculate Coherence
% different monkeys have data in different places
basedir = 'C:\Users\Julien\lab\data\MonkeyG_20150908_Session2_M1';
% if strcmp(name,'Jalapeno'), drive = 2;
% else drive = 3;
% end
% if ismac, basedir = '/Volumes';
% else basedir = 'C:\Users\Julien\lab\data\MonkeyG_20150908_Session2_M1';
% end
% basedir = fullfile(basedir,sprintf('data%i',drive),...
%     'PairedStimJalapeno','TDT_Blocks',name);

for s=1:length(sessions)
    date = sessions(s).date;
    session = sessions(s).session;
    array = sessions(s).arrays{1};
    
    for rec_block=1:6, % Each recording block
        
        % data directory
        datadir = fullfile(basedir,date,'Processed',...
            sprintf('Session%i',session),array,'RecordingBlocks');
        datafile = sprintf('RecBlock%i.mat',rec_block);
        f = load(fullfile(datadir,datafile),'samp_freq');
        
        % saving directory
        savedir = fullfile(basedir,date,'Processed',...
            sprintf('Session%i',session),array,'IntervaledCoherence_eLife');
        savefile = sprintf('block%i.mat',rec_block);
        if ~exist(savedir,'dir'), mkdir(savedir); end
%         if exist(fullfile(savedir,savefile),'file'), continue; end
        sf = matfile(fullfile(savedir,savefile),'Writable',true);
        
        % filter parameters
        samp_freq = f.samp_freq;
        
        % calculate cross spectrum
        Nwin = round(5*samp_freq);
        ovlp = 0;
        Nfft = 3000;
        
        coh1(1:96,1:96) = struct(...
            'mag',[]);
        coh2(1:96,1:96) = struct(...
            'mag',[]);
        
        fprintf('\n%s\n',date)
        fprintf('  - Session %i', session)
        
        fprintf('\n    * Rec Block %i of 6: ',rec_block)
        msg = sprintf('ch%02i <--> ch%02i',1,2);
        fprintf('%s',msg)
        
        for ch1 = 1:96,
            f = load(fullfile(datadir,datafile),sprintf('lfp_ch%02i',ch1));
            sig1 = f.(sprintf('lfp_ch%02i',ch1)); % load channel
            Nsig = length(sig1);
            NumWin = floor(Nsig / Nwin);
            N = Nsig - NumWin*Nwin; % for zero padding signal
            
            for ch2 = ch1+1:96,
                f = load(fullfile(datadir,datafile),sprintf('lfp_ch%02i',ch2));
                sig2 = f.(sprintf('lfp_ch%02i',ch2)); % load channel
                
                % coherence idx 1
                idx1 = repmat([ones(Nwin,1);zeros(Nwin,1)],floor(NumWin/2),1);
                idx1 = cat(1,idx1,zeros(N,1));
                idx1 = logical(idx1);
                [mag,~] = mscohere(sig1(idx1),sig2(idx1),hamming(Nwin),ovlp,Nfft,samp_freq);
                coh1(ch1,ch2).mag = mag;

                % coherence idx 2
                idx2 = repmat([zeros(Nwin,1);ones(Nwin,1)],floor(NumWin/2),1);
                idx2 = cat(1,idx2,zeros(N,1));
                idx2 = logical(idx2);
                [mag,F] = mscohere(sig1(idx2),sig2(idx2),hamming(Nwin),ovlp,Nfft,samp_freq);
                coh2(ch1,ch2).mag = mag;

                % output progress
                fprintf(repmat('\b',1,length(msg)))
                msg = sprintf('ch%02i <--> ch%02i',ch1,ch2);
                fprintf('%s',msg)
                
            end
        end
        fprintf('\n')
        
        try
            sf.coh1 = coh1;
            sf.coh2 = coh2;
            sf.freq = F;
        catch
            keyboard
        end
        
    end % recording block
end % session



