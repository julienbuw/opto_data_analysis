**Repository for data analysis of Elife "Targeted Cortical Reorganization..." paper**

---

## Repository Format

The data analysis is completed in dedicated jupyter notebooks, which call helper functions that are defined in coh_tools.py

The raw data for analysis is stored in the UCSF datashare Box, while the processed coherence data is stored by the Yazdan lab at UW.

## Regression Notebooks

The jupyter notebooks used for the regression presented at SfN19 are stored in timefreq_analysis/Regression

---