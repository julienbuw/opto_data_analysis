from scipy.signal import coherence as coh
from scipy.signal import hamming, csd
from sklearn.cluster import DBSCAN, MeanShift
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from difflib import SequenceMatcher as seqmatch
from math import ceil, log2
import numpy as np
import matplotlib.pyplot as plt
import h5py
import pickle as pkl
import os
from sklearn.base import BaseEstimator, RegressorMixin
import nitime.timeseries as ts
import nitime.analysis as nta



def extract_lfp(filename, want_fs=False):
    f = h5py.File(filename, 'r')
    keys = list(f.keys())
    keys.sort()
    i = 0
    make_signals = True
    for key in keys:
        if key.startswith('lfp'):
            if make_signals:
                signals = np.zeros((96, f[key].size))
                make_signals = False
            signals[i] = f[key][0]
            i += 1
    if want_fs:
        signals = (signals, f['samp_freq'][0,0])
    return signals

## Given a 96-channel matrix of signals, and the sampling frequency, 
## return a matrix of theta coherence values
## matrix is 3D, with [channel1, channel2]
def theta_coh(signals, sample_frequency):
    return band_coherence(signals, sample_frequency, 4, 7)

def nextpow2(x):
    return 2**ceil(log2(abs(x)))

## Return the entire coherence matrix, as [channel, channel, freq]
def coherence(signals, sample_frequency, want_f=False):
    Nwin = int(round(10 * sample_frequency))
    nfft = 16384
    nperseg = Nwin
    ovlp = int(Nwin/2)
    num_channels = signals.shape[0]
    mm = np.zeros((num_channels, num_channels, 8193))

    for from_index in range(num_channels):
        for to_index in range(from_index+1, num_channels):
            c = coh(signals[from_index,:], signals[to_index,:], sample_frequency, 
            	hamming(Nwin), nperseg=nperseg, nfft=nfft, noverlap=ovlp)
            mm[from_index,to_index] = c[1]
            mm[to_index, from_index] = c[1]
    mm += np.dstack([np.identity(num_channels)]*8193)
    if want_f:
        return (mm, c[0])
    return mm

# Function that takes in data matrix and returns matrix of coherences for 20 second partitions
# using windows of 2 seconds each, and discarding remainder of 2 seconds
def timefreq_coherence(signals, sample_frequency, partition_seconds=20, want_f=False, want_phase=False, nfft_multiplier=1):
#     num_partitions = int((signals.shape[1] / sample_frequency // partition_seconds) * 2 - 1)
    partition_size = partition_seconds * sample_frequency
    num_partitions = int(signals.shape[1] // partition_size)
    Nwin = int(partition_size // 10) # this yields number of samples for a tenth of partition seconds
    nperseg = Nwin
    nfft = nfft_multiplier*nextpow2(Nwin) #16384
    ovlp = int(Nwin/2)
    timefreq_mm = []
    for partition_i in range(num_partitions):
        partition = signals[:, partition_i*(int(partition_size)):(partition_i+1)*(int(partition_size))]
        mm = np.zeros((96, 96, int(nfft/2+1))) # 1025 is hardcoded and should be a function
        for from_index in range(96):
            for to_index in range(from_index+1, 96):
                if want_phase:
                    c = csd(partition[from_index,:], partition[to_index,:], sample_frequency, 
                        hamming(Nwin), nperseg=nperseg, nfft=nfft, noverlap=ovlp)
                    mm[from_index, to_index] = np.angle(c[1])
                    mm[to_index, from_index] = -np.angle(c[1])
                else:
                    c = coh(partition[from_index,:], partition[to_index,:], sample_frequency, 
                        hamming(Nwin), nperseg=nperseg, nfft=nfft, noverlap=ovlp)
                    mm[from_index, to_index] = c[1]
                    mm[to_index, from_index] = c[1]
        if not want_phase:
            mm += np.dstack([np.identity(96)]*int(nfft/2+1))
        timefreq_mm.append(mm)
    timefreq_mm = np.array(timefreq_mm)
    if want_f:
            return (timefreq_mm, c[0])   
    return timefreq_mm

# Function that takes in data matrix and returns matrix of directional Granger for 20 second partitions
def timefreq_granger(signals, sample_frequency, partition_seconds=20, want_f=False):
    partition_size = partition_seconds * sample_frequency
    num_partitions = int(signals.shape[1] // partition_size)
    num_channels = signals.shape[0]
    
    ij = []
    for i in range(num_channels):
        for j in range(num_channels):
            if i != j:
                ij.append((i, j))
    
    timefreq_mm = []
    for partition_i in range(num_partitions):
        partition = signals[:, partition_i*(int(partition_size)):(partition_i+1)*(int(partition_size))]
                    
        time_series = ts.TimeSeries(partition, sampling_rate=sample_frequency)
        G = nta.GrangerAnalyzer(time_series, order=10, ij=ij)
        Gxy = np.nan_to_num(G.causality_xy)
        freqs = G.frequencies
                
        Gxy += np.dstack([np.identity(num_channels)]*freqs.shape[0])
        
        timefreq_mm.append(Gxy)
    timefreq_mm = np.array(timefreq_mm)
    if want_f:
            return (timefreq_mm, freqs)   
    return timefreq_mm

def paths():
    print(__file__)
    print(os.path.abspath(__file__))
    print(os.path.dirname(__file__))
    print(os.path.basename(__file__))

# Data should be in format (time, channel, channel, freq)
def remove_bad_channels(data, keyname, bc_filepath, want_bc=False):
    bad_channels = pkl.load(open(bc_filepath, "rb"))
    key_bad_channels = sorted(bad_channels[keyname])
    for bad_channel in key_bad_channels[::-1]:
        data = np.delete(data, int(bad_channel-1), 1)
        data = np.delete(data, int(bad_channel-1), 2)
    if want_bc:
        return (data, bad_channels)
    return data

def get_m1_s1_matrices(m1_data, s1_data, good_channels, experiment):
    exp_m1_sites = np.trim_zeros(m1_data[m1_data["date"]==int(experiment[8:16])].values.tolist()[0][2:])
    exp_s1_sites = np.trim_zeros(s1_data[m1_data["date"]==int(experiment[8:16])].values.tolist()[0][2:])

    m1_matrix = np.zeros((len(good_channels[experiment]), len(good_channels[experiment])))
    s1_matrix = np.zeros((len(good_channels[experiment]), len(good_channels[experiment])))

    for i, chan_i in enumerate(good_channels[experiment]):
        for j, chan_j in enumerate(good_channels[experiment]):
            if (chan_i in exp_m1_sites) and (chan_j in exp_m1_sites):
                m1_matrix[i, j] = 1
            if (chan_i in exp_s1_sites) and (chan_j in exp_s1_sites):
                s1_matrix[i, j] = 1
    return [m1_matrix, s1_matrix]

## Return the entire coherency matrix, as [channel, channel, freq]
def coherency(signals, sample_frequency, want_f=False):
    Nwin = int(round(10 * sample_frequency))
    nfft = 16384
    nperseg = Nwin
    ovlp = int(Nwin/2)
    mm = np.zeros((96, 96, 8193), dtype="Complex32")

    for from_index in range(96):
        for to_index in range(from_index+1, 96):
            c = csd(signals[from_index,:], signals[to_index,:], sample_frequency, 
                hamming(Nwin), nperseg=nperseg, nfft=nfft, noverlap=ovlp)
            mm[from_index,to_index] = c[1] #np.angle(c[1]) EDIT: Because need coherence and angle
            mm[to_index, from_index] = c[1] #-np.angle(c[1])
    if want_f:
        return (mm, c[0])
    return mm

def polar2cart(theta, r):
    return r * np.exp( 1j * theta )

def cart2polar(z):
    return (np.angle(z), np.abs(z))

def moving_average(a, n=3) :
    # add first and last element
#     num_add = n // 2
    where_add = 0
    for num in range(n-1):
        if where_add:
            a = np.insert(a, 0, a[0])
        else:
            a = np.append(a, a[-1])
        where_add = not where_add
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

## Return coherence matrix for a specific frequency range
def band_coherence(signals, sample_frequency, lowf, highf):
    c, freqs = coherence(signals, sample_frequency, want_f=True)
    start_index, end_index = get_freq_band_indices(freqs, lowf, highf)
    band_coh = c[:,:,start_index:end_index]
    band_means_coh = band_coh.mean(axis=2)
    return band_means_coh

## Return coherency matrix for a specific frequency range
def band_coherency(signals, sample_frequency, lowf, highf):
    c, freqs = coherency(signals, sample_frequency, want_f=True)
    start_index, end_index = get_freq_band_indices(freqs, lowf, highf)
    band_coh = c[:,:,start_index:end_index]
    band_means_coh = band_coh.mean(axis=2)
    return band_means_coh

## Return indixes of beginning and end of a frequency band for a given (sorted) 
## frequency array
def get_freq_band_indices(frequencies, low, high):
    first = True
    for i in range(len(frequencies)):
        if first and frequencies[i] > low:
            low_index = i
            first = False
        if frequencies[i] > high:
            high_index = i
            break
    return [low_index, high_index]

## Given a similarity matrix, perform DBSCAN clustering, then project the data
## along its 2 largest PCs, then plot the clustered data on the 2-PC space
def pca_dbscan_cluster(data, savename=None):
    labels = DBSCAN().fit_predict(data)
    pca_projection = PCA(n_components=2).fit_transform(data)
    plt.scatter(pca_projection[:,0], pca_projection[:,1], c=labels, 
    	s=150*np.ones(labels.size))
    plt.colorbar()
    if savename:
        plt.savefig(str(savename), dpi=128)
    plt.close()
    return labels

## Given a similarity matrix, perform meanshift clustering, then project the data
## along its 2 largest PCs, then plot the clustered data on the 2-PC space
def pca_meanshift_cluster(data, savename=None):
    labels = MeanShift().fit_predict(data)
    pca_projection = PCA(n_components=2).fit_transform(data)
    plt.scatter(pca_projection[:,0], pca_projection[:,1], c=labels, 
    	s=150*np.ones(labels.size))
    plt.colorbar()
    if savename:
        plt.savefig(str(savename), dpi=128)
    plt.close()
    return labels

def plot_tsne(data, labels, savename, tsne_data=None):
    if tsne_data is None:
        tsne_data = TSNE().fit_transform(data)
    plt.scatter(tsne_data[:, 0], tsne_data[:, 1], c=labels, 
    	s=150*np.ones(labels.size))
    plt.colorbar()
    plt.savefig(str(savename), dpi=128)
    plt.close()
    return tsne_data

########## M1/S1 IN PROGRESS #################
def map_to_ecog(labels, savename=None, cmap="viridis", laser_from=None, laser_to=None, csv_dir="", 
        center=False, m1_sites=None, s1_sites=None, spread=1, minval=None, maxval=None, plot=None, s=200,
        want_data=False, good_electrode_list=[], marker="s"):
    ecog_numbers = np.loadtxt(csv_dir+"ecog_number.csv", delimiter=",", dtype=int).flatten()
    ecog_xs = np.loadtxt(csv_dir+"ecog_x_coords.csv", delimiter=",").flatten()
    ecog_ys = np.loadtxt(csv_dir+"ecog_y_coords.csv", delimiter=",").flatten()
    xs = []
    ys = []

    m1_xs = []
    m1_ys = []
    s1_xs = []
    s1_ys = []

    laser_from_coords = []
    laser_to_coords = []
#     k = 0 # Index of electrode in csv
    remapped_labels = []
    for k, electrode in enumerate(ecog_numbers):
        if electrode > 0:
            if good_electrode_list != []:
                if electrode not in good_electrode_list:
                    continue
            xs.append(ecog_xs[k])
            ys.append(ecog_ys[k])
            remapped_labels.append(labels[electrode-1])
            if laser_from:
                if laser_from == electrode:
                    laser_from_coords = [ecog_xs[k], ecog_ys[k]]
            if laser_to:
                if laser_to == electrode:
                    laser_to_coords = [ecog_xs[k], ecog_ys[k]]
            if m1_sites:
                if electrode in m1_sites:
                    m1_xs.append([ecog_xs[k], ecog_ys[k]])
                    m1_ys.append([ecog_xs[k], ecog_ys[k]])
            if s1_sites:
                if electrode in s1_sites:
                    s1_xs.append([ecog_xs[k], ecog_ys[k]])
                    s1_ys.append([ecog_xs[k], ecog_ys[k]])
#         k += 1
    # how do I plot this but also not affect the colorbar? Maybe I dont need the colorbar?
    if m1_sites:
        plt.scatter(m1_xs, m1_ys, c='g', s=s*np.ones(96))
    if s1_sites:
        plt.scatter(s1_xs, s1_ys, c='o', s=s*np.ones(96))

    if center:
        plt.scatter(xs, ys, c=remapped_labels, s=s * np.ones(labels.size), marker=marker,
        	cmap=cmap, vmin=-spread, vmax=spread)
        plt.colorbar()
    if maxval and plot:
        col = plot.scatter(xs, ys, c=remapped_labels, s=s*np.ones(labels.size), marker=marker,
            cmap=cmap, vmin=minval, vmax=maxval)
    elif plot:
        col = plot.scatter(xs, ys, c=remapped_labels, s=s*np.ones(labels.size), marker=marker,
            cmap=cmap, vmin=minval, vmax=maxval)
    elif not want_data:
        col = plt.scatter(xs, ys, c=remapped_labels, s=s*np.ones(labels.size), marker=marker,
            cmap=cmap)
    # plt.colorbar()
    if laser_from_coords:
        plt.scatter(laser_from_coords[0], laser_from_coords[1], marker=marker, 
        	facecolors='none', edgecolors='r', s=s*np.ones(2), linewidth=3)
    if laser_to_coords:
        plt.scatter(laser_to_coords[0], laser_to_coords[1], marker=marker, 
        	facecolors='none', edgecolors='b', s=s*np.ones(2), linewidth=3)
    if savename:
        plt.savefig(str(savename), dpi=128)
        plt.close()
    if want_data:
        return [xs, ys, remapped_labels]
    return col

def plotter(coherences, savenamebase, ecog_cmap="viridis", laser_from=None, 
	laser_to=None, want_meanshift_labels=False, old_meanshift_labels=None):
    labels = pca_dbscan_cluster(coherences, savenamebase+"_pca_dbscan.png")
    tsne_data = plot_tsne(coherences, labels, savenamebase+"_tsne_dbscan.png")
    map_to_ecog(labels, savenamebase+"_ecog_dbscan.png", cmap=ecog_cmap, 
    	laser_from=laser_from, laser_to=laser_to)

    # for meanshift make sure you use the same clusters
    labels = pca_meanshift_cluster(coherences, savenamebase+"_pca_meanshift.png")
    if old_meanshift_labels:
        labels = cluster_remap(old_meanshift_labels, labels)
    plot_tsne(coherences, labels, savenamebase+"_tsne_meanshift.png",
    	tsne_data=tsne_data)
    map_to_ecog(labels, savenamebase+"_ecog_meanshift.png", cmap=ecog_cmap, 
    	laser_from=laser_from, laser_to=laser_to)
    if want_meanshift_labels:
        return labels

########## IN PROGRESS #################
def sub_plotter(coherences, savenamebase, ecog_cmap="viridis", laser_from=None,
	laser_to=None):
    fig, ax = plt.subplots()

    labels = pca_dbscan_cluster(coherences, savenamebase+"_pca_dbscan.png")
    tsne_data = plot_tsne(coherences, labels, savenamebase+"_tsne_dbscan.png")
    map_to_ecog(labels, savenamebase+"_ecog_dbscan.png", cmap=ecog_cmap,
    	laser_from=laser_from, laser_to=laser_to)

    labels = pca_meanshift_cluster(coherences, savenamebase+"_pca_meanshift.png")
    plot_tsne(coherences, labels, savenamebase+"_tsne_meanshift.png",
    	tsne_data=tsne_data)
    map_to_ecog(labels, savenamebase+"_ecog_meanshift.png", cmap=ecog_cmap,
    	laser_from=laser_from, laser_to=laser_to)


def get_band_range(band):
    bands = {"delta": [0.001,4], "theta": [4, 7], "alpha": [8,12], "beta": [12,30], 
    	"gamma": [30, 70], "high_gamma": [70, 199]}
    assert band in bands, "Must specify a valid frequency band."
    return bands[band]

def labels_to_clusters(l):
    indices = {i: [] for i in range(len(set(l)))}
    for c in range(len(l)):
        indices[l[c]].append(c)
    return indices

def get_coh_diff_mag(m):
    diff_mag_m = np.zeros(m.shape)
    for i in range(m.shape[0]):
        for j in range(i):
            diff = m[i, :] - m[j, :]
            diff_mag_m[i, j] = np.mean(np.abs(diff)) # np.linalg.norm(diff) / (m.shape[0]**.5)
    diff_mag_m += diff_mag_m.T
    return diff_mag_m

def get_coh_sum_mag(m):
    sum_mag_m = np.zeros(m.shape)
    for i in range(m.shape[0]):
        for j in range(i):
            sum_ = m[i, :] + m[j, :]
            sum_mag_m[i, j] = np.linalg.norm(sum_) / (m.shape[0]**.5)
    sum_mag_m += sum_mag_m.T
    return sum_mag_m

def get_pair_net_coh(m):
    m_halfmeans = m.mean(0)
    net_m = np.zeros(m.shape)
    for i in range(m.shape[0]):
        for j in range(i):
            net_m[i, j] = m_halfmeans[i] + m_halfmeans[j]
            net_m[j, i] = net_m[i, j]
    return net_m

def get_avg_coh_to_stim(m, site1, site2):
    avg_coh_to_stim = np.zeros(m.shape)
    for i in range(m.shape[0]):
        for j in range(i):
            avg_coh_to_stim[i, j] = np.mean([m[i, site1], m[j, site1], m[i, site2], m[j, site2]])
            avg_coh_to_stim[j, i] = avg_coh_to_stim[i, j]
    return avg_coh_to_stim

def get_avg_diff_12_stim(m, site1, site2):
    avg_diff_btw_stim = np.zeros(m.shape)
    for i in range(m.shape[0]):
        for j in range(i):
            avg_diff_btw_stim[i, j] = np.mean([m[i, site1] - m[i, site2], m[j, site1] - m[j, site2]])
            avg_diff_btw_stim[j, i] = avg_diff_btw_stim[i, j]
    return avg_diff_btw_stim

def get_avg_stim_diff_btw_elec(m, site1, site2):
    avg_stim_diff_btw_elec = np.zeros(m.shape)
    for i in range(m.shape[0]):
        for j in range(i):
            avg_stim_diff_btw_elec[i, j] = np.mean([np.abs(m[i, site1] - m[j, site1]), np.abs(m[i, site2] - m[j, site2])])
            avg_stim_diff_btw_elec[j, i] = avg_stim_diff_btw_elec[i, j]
    return avg_stim_diff_btw_elec

########## IN PROGRESS #################
# assumes that to_labels has more clusters than from_labels
def cluster_remap(to_labels, from_labels):
    mapping = {}
    to_clusters = labels_to_clusters(to_labels)
    from_clusters = labels_to_clusters(from_labels)
    to_vals = list(to_clusters.keys())
    to_vals.sort(key=lambda x: -len(to_clusters[x]))
    from_vals = list(from_clusters.keys())
    from_vals.sort(key=lambda x: -len(from_clusters[x]))
    for to_val in to_vals:
        sims = []
        for from_val in from_vals:
            sm=seqmatch(None, to_clusters[to_val], from_clusters[from_val])
            sims.append(sm.ratio())
        # add largest similarity to mapping
        mapping[from_vals[sims.index(max(sims))]] = to_val
        from_vals.pop(sims.index(max(sims)))
    new_from_labels = [mapping[c] for c in from_labels]
    return new_from_labels
